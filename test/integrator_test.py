import os

import integrator.integrator as integrator
import integrator.helpers as helpers
import unittest


logger = logging.getLogger()

vtex_url_base = "https://worldwidegolfqa.myvtex.com/api"
app_key = os.getenv("VTEX_APP_KEY")
app_token = os.getenv("VTEX_APP_TOKEN")
vtex_headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-VTEX-API-AppKey": app_key,
    "X-VTEX-API-AppToken": app_token
}

akeneo_url_base = "https://wwgs-sandbox.cloud.akeneo.com/api"
akeneo_username = os.getenv("WWGS_Akeneo_Username")
akeneo_password = os.getenv("WWGS_Akeneo_Password")
akeneo_auth = os.getenv("WWGS_Base_64")
akeneo_token = ""


class IntegratorTest(unittest.TestCase):

    @staticmethod
    def initialize():
        helpers.setup_logger(logger, "integrator_tester")
        global akeneo_token
        akeneo_token = helpers.get_auth(akeneo_url_base, akeneo_auth, akeneo_username, akeneo_password)

    # Category
    def get_all_akeneo_categories(self):
        # Ensure 'get all akeneo categories' returns a list of Category objects
        akeneo_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {akeneo_token}"}
        akeneo_categories = integrator.Category.get_all_akeneo_categories(akeneo_headers)
        self.assertIsInstance(akeneo_categories, list)
        self.assertIsInstance(akeneo_categories[0], integrator.Category)

    def get_vtex_categories(self):
        # Ensure 'get vtex categories returns a dict with keys of type string
        vtex_categories = integrator.Category.get_vtex_categories()
        self.assertIsInstance(vtex_categories, dict)
        first_category = vtex_categories.popitem()
        logger.info(first_category)
        self.assertIsInstance(first_category, (str, str))

    # Attribute
    def attribute_constructor_equals(self):
        attr_1 = integrator.Attribute("test", "Only a Test", 1)
        attr_2 = integrator.Attribute("test", "Only a Test", 2)
        self.assertEquals(attr_1, attr_2)

    def attribute_constructor_not_equals(self):
        attr_1 = integrator.Attribute("test", "Only a Test!", 1)
        attr_2 = integrator.Attribute("test", "Only a Test", 2)
        self.assertNotEquals(attr_1, attr_2)

    def get_akeneo_attributes(self):
        akeneo_attrs = integrator.Attribute.get_akeneo_attributes()
        self.assertIsInstance(akeneo_attrs, set)
        self.assertIsInstance(akeneo_attrs.pop(), integrator.Attribute)

    def get_vtex_fields(self):
        vtex_fields = integrator.Attribute.get_vtex_specifications()
        self.assertIsInstance(vtex_fields, set)
        self.assertIsInstance(vtex_fields.pop(), integrator.Attribute)

    # SKU


if __name__ == '__main__':
    unittest.main()
