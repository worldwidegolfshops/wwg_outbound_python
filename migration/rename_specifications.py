import logging
import os
from datetime import datetime
import json
import time

import requests

logger = logging.getLogger()
log_filename = ""

number_of_attempts = 5
retry_wait = 30  # Time in seconds to wait between retries

vtex_env = "Production"
if vtex_env == "QA":
    vtex_url_base = "https://worldwidegolfqa.myvtex.com/api"
    app_key = os.getenv("VTEX_APP_KEY")
    app_token = os.getenv("VTEX_APP_TOKEN")
elif vtex_env == "Production":
    vtex_url_base = "https://worldwidegolf.myvtex.com/api"
    app_key = os.getenv("VTEX_APP_KEY_PROD")
    app_token = os.getenv("VTEX_APP_TOKEN_PROD")
else:
    raise ChildProcessError("VTEX Environment not set correctly in config.  Valid values are: QA, Production")

vtex_headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-VTEX-API-AppKey": app_key,
    "X-VTEX-API-AppToken": app_token
}

vtex_specs = set()
spec_name_to_spec = {}
category_name_to_id = {}


def setup_logger(filename):
    timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    log_format = "%(asctime)s|%(levelname)s|%(threadName)s|%(message)s"
    global log_filename
    log_filename = f"./logs/{filename}_{timestamp}.log"

    logging.basicConfig(format=log_format)
    formatter = logging.Formatter(log_format)
    logger.setLevel(logging.INFO)

    info_log_channel = logging.FileHandler(log_filename)
    info_log_channel.setLevel(logging.INFO)
    info_log_channel.setFormatter(formatter)
    logger.addHandler(info_log_channel)


def call_service(url, headers, method, data=None, params=None, attempt=1, handle_404=False):
    response = requests.request(method=method, url=url, headers=headers, json=data, params=params)
    if response.ok:
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return {}
    elif 400 <= response.status_code < 500 and response.status_code != 429:  # 429 is a rate limit response
        if handle_404 and response.status_code == 404:
            return None
        else:
            raise ChildProcessError(
                f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
    else:
        logger.error(f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
        if attempt < number_of_attempts:
            logger.info(f"Attempt {attempt}. Waiting {retry_wait} seconds and then retrying: {url}")
            time.sleep(retry_wait)
            return call_service(url, headers, method, data, params, attempt + 1)
        else:
            raise ChildProcessError(
                f"Unable to get a successful response after {number_of_attempts} attempts on: {url}")


def get_subcategories(category):
    subcategories = {}
    for subcategory in category['children']:
        subcategories[subcategory['name']] = get_subcategories(subcategory)
        category_name_to_id[subcategory['name']] = subcategory['id']

    return subcategories


def get_vtex_categories():
    category_tree = {}
    url = f"{vtex_url_base}/catalog_system/pvt/category/tree/10"
    category_json = call_service(url, vtex_headers, "GET")
    for root_category in category_json:
        category_tree[root_category['name']] = get_subcategories(root_category)
        category_name_to_id[root_category['name']] = root_category['id']


def get_specifications():
    logger.info("Get VTEX Categories")
    get_vtex_categories()
    category_name_to_id["ROOT"] = 0
    for category in category_name_to_id:
        logger.info(f"Getting specs for category: {category}")
        category_id = category_name_to_id[category]
        url = f"{vtex_url_base}/catalog_system/pub/specification/field/listByCategoryId/{category_id}"
        specifications = call_service(url, vtex_headers, "GET")
        for specification in specifications:
            spec_name_to_spec[specification["Name"]] = specification
            vtex_specs.add(specification["FieldId"])


def rename_specifications():
    logger.info(f"There are {len(vtex_specs)} specs in VTEX to rename. Let's do it")

    for specification_id in vtex_specs:
        url = f"{vtex_url_base}/catalog/pvt/specification/{specification_id}"
        specification = call_service(url, vtex_headers, "GET")
        specification["Name"] = f"{specification['Name']}-"

        call_service(url, vtex_headers, "PUT", data=specification)
        logger.info(f"Renamed Spec: {specification}")


def main():
    setup_logger("specification_renamer")
    logger.info(f"Begin Specification Renaming on VTEX {vtex_env}")

    get_specifications()
    rename_specifications()


if __name__ == '__main__':
    main()
