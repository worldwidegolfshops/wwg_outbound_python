# Products in VTEX that can’t be linked to Product Models in Akeneo are inactivated
# Skus in VTEX that can’t be linked to SKUs in Akeneo are inactivated
# All Specifications in VTEX are inactivated
# Sync Specifications from Akeneo to VTEX
# Sync Products from Akeneo to VTEX
# Sync SKUs from Akeneo to VTEX

import os
import logging
import time

import integrator.integrator as integrator
import integrator.helpers as helpers
import inactivate_products
import inactivate_skus
import rename_specifications


logger = logging.getLogger()

akeneo_url_base = "https://wwgs-sandbox.cloud.akeneo.com/api"
akeneo_username = os.getenv("WWGS_Akeneo_Username")
akeneo_password = os.getenv("WWGS_Akeneo_Password")
akeneo_auth = os.getenv("WWGS_Base_64")
access_token = helpers.get_auth(akeneo_url_base, akeneo_auth, akeneo_username, akeneo_password)
logger.info(f"Access Token: {access_token}")


def sync_specifications():
    logger.info("Begin Syncing Specifications")
    integrator.akeneo_token = helpers.get_auth(akeneo_url_base, akeneo_auth, akeneo_username, akeneo_password)
    integrator.akeneo_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {integrator.akeneo_token}"}
    akeneo_attributes = integrator.Attribute.get_akeneo_attributes()

    for attr in akeneo_attributes:
        if attr.label == "Size":
            logger.info(f"{attr.code} {attr.item_type}")

    attributes = {"SKU": {}, "PRODUCT": {}}
    for attr in akeneo_attributes:
        if attr.attribute_type in attributes[attr.item_type]:
            attributes[attr.item_type][attr.attribute_type].append(attr.label)
        else:
            attributes[attr.item_type][attr.attribute_type] = [attr.label]

    integrator.Attribute.process_specifications_update([], akeneo_attributes, [])
    logger.info("Syncing Specifications Complete")


def main():
    helpers.setup_logger(logger, "migration")
    logger.info("Begin Migration")

    # logger.info("Begin Inactivation")
    # akeneo_skus = inactivate_skus.main()
    # akeneo_models = inactivate_products.main()
    # inactivate_specifications.main()
    # logger.info("Inactivation Complete")

    # logger.info("Waiting around 30 minutes to give VTEX time to update all the names and flags")
    # time.sleep(1776)

    logger.info("Begin Syncing")
    sync_specifications()
    # logger.info("Syncing Complete")

    logger.info("Migration Complete")


if __name__ == '__main__':
    main()
