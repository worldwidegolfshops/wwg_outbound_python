import threading
import concurrent.futures

from migration_helpers import *


def get_vtex_skus():
    logger.info("Getting VTEX SKUs")
    page_number = 1
    vtex_url = f"{vtex_url_base}/catalog_system/pvt/sku/stockkeepingunitids"
    vtex_params = {"page": f"{page_number}", "pagesize": "1000"}
    response = call_service(vtex_url, vtex_headers, "GET", params=vtex_params)
    vtex_skus.extend(response)

    while response:
        page_number += 1
        vtex_params = {"page": f"{page_number}", "pagesize": "1000"}
        logger.info(f"Getting VTEX page {page_number}")
        response = call_service(vtex_url, vtex_headers, "GET", params=vtex_params)
        vtex_skus.extend(response)

    logger.info(f"There are {len(vtex_skus)} skus in VTEX")


def get_all_akeneo_skus():
    logger.info("Getting Akeneo SKUs")
    akeneo_sku_url = f"{akeneo_url_base}/rest/v1/products?pagination_type=search_after&limit=100"
    sku_links = get_akeneo_skus(akeneo_sku_url)
    while "next" in sku_links:
        sku_links = get_akeneo_skus(sku_links["next"]["href"])

    logger.info(f"There are {len(akeneo_skus)} Akeneo SKUs.")


def get_akeneo_skus(sku_url):
    skus_response = call_service(sku_url, akeneo_headers, "GET")
    for sku_data in skus_response["_embedded"]["items"]:
        akeneo_skus.append(int(sku_data["identifier"]))
    logger.info(f"Got {len(akeneo_skus)} SKUs from Akeneo")
    return skus_response["_links"]


def check_vtex_to_akeneo_ids():
    logger.info("Check VTEX SKU Ids against Akeneo SKU Ids to determine matches and non-matches")
    for vtex_sku_id in vtex_skus:
        if vtex_sku_id in akeneo_skus:
            matches.append(vtex_sku_id)
        else:
            non_matches.append(vtex_sku_id)


def write_to_files():
    with open(sku_non_matches_filename, "w") as file:
        for non_match in non_matches:
            file.write(f"{non_match}\n")

    with open(sku_matches_filename, "w") as file:
        for match in matches:
            file.write(f"{match}\n")


def get_vtex_sku(akeneo_id) -> dict:
    vtex_url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit?refId={akeneo_id}"
    response = call_service(vtex_url, vtex_headers, "GET", handle_404=True)
    if response:
        return response
    else:
        return {"Id": None}


def inactivate_sku(ref_id):
    sku = get_vtex_sku(ref_id)

    if not sku["Id"]:
        logger.info(f"SKU not found in VTEX: {ref_id}")
        return

    if not sku["IsActive"]:
        logger.info(f"SKU is already inactive: {ref_id}")
        return

    sku["IsActive"] = False
    sku["ActivateIfPossible"] = False
    url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{sku['Id']}"
    call_service(url, vtex_headers, "PUT", data=sku)
    logger.info(f"Inactivated SKU: {ref_id}")


def inactivate_skus():
    global non_matches
    non_matches = []
    with open(sku_non_matches_filename, "r") as file:
        for line in file:
            non_matches.append(line.strip())

    number_of_skus_to_inactivate = len(non_matches)
    logger.info(f"There are {number_of_skus_to_inactivate} SKUs to inactivate. Let's do it")

    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        executor.map(inactivate_sku, non_matches)

    # for count, non_match in enumerate(non_matches):
    #     inactivate_sku(non_match)
    #     logger.info(f"{count} - {count / number_of_skus_to_inactivate * 100 }% - Inactivated SKU: {non_match}")


def read_files():
    with open("./logs/akeneo_skus_prod.txt", "r") as file:
        for line in file:
            akeneo_skus.append(int(line.strip()))

    with open("./logs/vtex_skus_prod.txt", "r") as file:
        for line in file:
            vtex_skus.append(int(line.strip()))


def main():
    setup_logger("sku_inactivator")
    logger.info("Begin SKU Processing")

    set_auth()

    # t1 = threading.Thread(target=get_all_akeneo_skus)
    # t1.start()
    # t2 = threading.Thread(target=get_vtex_skus)
    # t2.start()
    # t2.join()
    # t1.join()
    #
    # # read_files()
    # check_vtex_to_akeneo_ids()
    #
    # logger.info(f"There are {len(matches)} matches and {len(non_matches)} non-matches.")
    #
    # write_to_files()

    logger.info("Inactivating the non-matches")
    inactivate_skus()

    logger.info("SKU Processing Complete")


if __name__ == '__main__':
    main()
