import concurrent.futures
import threading

from migration_helpers import *


def get_all_akeneo_models():
    logger.info("Getting Akeneo Models")
    akeneo_model_url = f"{akeneo_url_base}/rest/v1/product-models?pagination_type=search_after&limit=100"
    product_links = get_akeneo_models(akeneo_model_url)
    while "next" in product_links:
        product_links = get_akeneo_models(product_links["next"]["href"])

    logger.info(f"There are {len(akeneo_models)} Akeneo Models.")


def get_akeneo_models(url):
    products_response = call_service(url, akeneo_headers, "GET")
    for product_data in products_response["_embedded"]["items"]:
        akeneo_models.append(product_data["code"])
    logger.info(f"Got {len(akeneo_models)} Models from Akeneo")
    return products_response["_links"]


def get_root_categories():
    root_categories = []

    url = f"{vtex_url_base}/catalog_system/pub/category/tree/0"
    categories = call_service(url, vtex_headers, "GET")
    for category in categories:
        root_categories.append(category["id"])

    return root_categories


def get_vtex_products_in_category(category: str):
    url = f"{vtex_url_base}/catalog_system/pvt/products/GetProductAndSkuIds"
    _from = 1
    _to = 50
    vtex_params = {"categoryId": f"{category}", "_from": f"{_from}", "_to": f"{_to}"}
    product_response = call_service(url, vtex_headers, "GET", params=vtex_params)
    total = product_response["range"]["total"]

    while total > _from:
        vtex_params = {"categoryId": f"{category}", "_from": f"{_from}", "_to": f"{_to}"}
        product_response = call_service(url, vtex_headers, "GET", params=vtex_params)
        for product_data in product_response["data"]:
            vtex_products.append(product_data)

        _from += 50
        _to += 50


def get_vtex_products():
    logger.info("Getting VTEX Products")
    categories = get_root_categories()
    with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
        executor.map(get_vtex_products_in_category, categories)

    # for category in categories:
    #     get_vtex_products_in_category(category)

    logger.info(f"There are {(len(vtex_products))} VTEX Products")
    get_vtex_ref_ids()


def get_vtex_ref_id(product):
    url = f"{vtex_url_base}/catalog/pvt/product/{product}"
    response = call_service(url, vtex_headers, "GET")
    vtex_ref_ids.append(response["RefId"])


def get_vtex_ref_ids():
    logger.info("Get all VTEX Product RefIds")
    with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
        executor.map(get_vtex_ref_id, vtex_products)


def check_ref_ids_to_models():
    logger.info("Check VTEX Product RefIds against Akeneo Model Codes to determines matches and non-matches")
    for vtex_ref_id in vtex_ref_ids:
        if vtex_ref_id in akeneo_models:
            matches.append(vtex_ref_id)
        else:
            non_matches.append(vtex_ref_id)


def write_to_files():
    with open(product_non_matches_filename, "w") as file:
        for non_match in non_matches:
            file.write(f"{non_match}\n")

    with open(product_matches_filename, "w") as file:
        for match in matches:
            file.write(f"{match}\n")


def get_vtex_product(ref_id):
    vtex_url = f"{vtex_url_base}/catalog_system/pvt/products/productgetbyrefid/{ref_id}"
    response = call_service(vtex_url, vtex_headers, "GET", handle_404=True)
    if response:
        return response
    else:
        return {"Id": None}


def inactivate_product(ref_id):
    product = get_vtex_product(ref_id)

    if not product["Id"]:
        logger.info(f"Product not found in VTEX: {ref_id}")
        return

    if not product["IsActive"]:
        logger.info(f"Product is already inactive: {ref_id}")
        return

    product["IsActive"] = False
    url = f"{vtex_url_base}/catalog/pvt/product/{product['Id']}"
    call_service(url, vtex_headers, "PUT", data=product)


def inactivate_products():
    global non_matches
    non_matches = []
    with open(product_non_matches_filename, "r") as file:
        for line in file:
            non_matches.append(line.strip())

    number_of_products_to_inactivate = len(non_matches)
    logger.info(f"There are {number_of_products_to_inactivate} Products to inactivate. Let's do it")

    for count, non_match in enumerate(non_matches):
        inactivate_product(non_match)
        logger.info(f"{count} - {count / number_of_products_to_inactivate * 100}% - Inactivated Product: {non_match}")


def main():
    setup_logger("product_inactivator")
    logger.info("Begin Product Processing")

    t1 = threading.Thread(target=get_all_akeneo_models)
    t1.start()
    t2 = threading.Thread(target=get_vtex_products)
    t2.start()
    t1.join()
    t2.join()

    # get_all_akeneo_models()
    # get_vtex_products()

    check_ref_ids_to_models()
    write_to_files()
    logger.info(f"There are {len(matches)} matches and {len(non_matches)} non-matches.")

    logger.info("Inactivating the non-matches")
    inactivate_products()

    logger.info("Product Processing Complete")


if __name__ == '__main__':
    main()
