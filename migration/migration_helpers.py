import json
import os
import logging
import time
from datetime import datetime

import requests

logger = logging.getLogger()
log_filename = ""

number_of_attempts = 5
retry_wait = 30  # Time in seconds to wait between retries

akeneo_env = "Production"
if akeneo_env == "Sandbox":
    akeneo_url_base = "https://wwgs-sandbox.cloud.akeneo.com/api"
    akeneo_username = os.getenv("WWGS_Akeneo_Username")
    akeneo_password = os.getenv("WWGS_Akeneo_Password")
    akeneo_auth = os.getenv("WWGS_Base_64")
elif akeneo_env == "Production":
    akeneo_url_base = "https://wwgs.cloud.akeneo.com/api"
    akeneo_username = os.getenv("WWGS_Akeneo_Username_PROD")
    akeneo_password = os.getenv("WWGS_Akeneo_Password_PROD")
    akeneo_auth = os.getenv("WWGS_Base_64_PROD")
else:
    raise ChildProcessError("Akeneo Environment not set correctly in config. Valid values are: Sandbox, Production")

akeneo_token = ""
akeneo_headers = ""

auth_url = f"{akeneo_url_base}/oauth/v1/token"
auth_headers = {"Content-Type": "application/json", "Authorization": akeneo_auth}
auth_body = {"grant_type": "password", "username": akeneo_username, "password": akeneo_password}

vtex_env = "Production"
if vtex_env == "QA":
    vtex_url_base = "https://worldwidegolfqa.myvtex.com/api"
    app_key = os.getenv("VTEX_APP_KEY")
    app_token = os.getenv("VTEX_APP_TOKEN")
elif vtex_env == "Production":
    vtex_url_base = "https://worldwidegolf.myvtex.com/api"
    app_key = os.getenv("VTEX_APP_KEY_PROD")
    app_token = os.getenv("VTEX_APP_TOKEN_PROD")
else:
    raise ChildProcessError("VTEX Environment not set correctly in config.  Valid values are: QA, Production")

vtex_headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-VTEX-API-AppKey": app_key,
    "X-VTEX-API-AppToken": app_token
}

matches = []
non_matches = []

# SKU
akeneo_skus = []
vtex_skus = []
sku_matches_filename = f"./logs/matches_SKU_{vtex_env}.txt"
sku_non_matches_filename = f"./logs/non_matches_SKU_{vtex_env}.txt"

# Product
akeneo_models = []
vtex_products = []
vtex_ref_ids = []
product_matches_filename = f"./logs/matches_Product_{vtex_env}.txt"
product_non_matches_filename = f"./logs/non_matches_Product_{vtex_env}.txt"


def setup_logger(filename):
    timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    log_format = "%(asctime)s|%(levelname)s|%(threadName)s|%(message)s"
    global log_filename
    log_filename = f"./logs/{filename}_{timestamp}.log"

    logging.basicConfig(format=log_format)
    formatter = logging.Formatter(log_format)
    logger.setLevel(logging.INFO)

    info_log_channel = logging.FileHandler(log_filename)
    info_log_channel.setLevel(logging.INFO)
    info_log_channel.setFormatter(formatter)
    logger.addHandler(info_log_channel)


def call_service(url, headers, method, data=None, params=None, attempt=1, handle_404=False):
    response = requests.request(method=method, url=url, headers=headers, json=data, params=params)
    if response.ok:
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return {}
    elif 400 <= response.status_code < 500 and response.status_code != 429:  # 429 is a rate limit response
        if response.status_code == 401:  # 401 is an invalid authentication response
            set_auth()
            return call_service(url, akeneo_headers, method, data, params, attempt + 1)
        elif handle_404 and response.status_code == 404:
            return None
        else:
            raise ChildProcessError(
                f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
    else:
        logger.error(f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
        if attempt < number_of_attempts:
            logger.info(f"Attempt {attempt}. Waiting {retry_wait} seconds and then retrying: {url}")
            time.sleep(retry_wait)
            return call_service(url, headers, method, data, params, attempt + 1)
        else:
            raise ChildProcessError(
                f"Unable to get a successful response after {number_of_attempts} attempts on: {url}")


def set_auth():
    logger.info("Authenticating with Akeneo")

    global akeneo_token
    global akeneo_headers

    auth_response = call_service(auth_url, auth_headers, "POST", data=auth_body)
    akeneo_token = auth_response["access_token"]
    akeneo_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {akeneo_token}"}
