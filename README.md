# Akeneo Integration #

This repository holds the code used for integrating Akeneo with other systems.

## Architecture Overview

The VTEX integration runs once per day, 
gets all the SKUs and Models that have changed in Akeneo since the last run, 
and pushes the active items to VTEX.

## Who do I talk to? ###

- Cory Eft (ceft@infoverity.com)
- Kurt Kissinger (kkissinger@infoverity.com)
