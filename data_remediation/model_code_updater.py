import threading
import concurrent.futures
import os
from functools import partial

import requests

file = "C:/Users/KKissinger/OneDrive - Infoverity, Inc/Infoverity/PROJECTS/WorldWideGolf/model_code_updates.txt"
url_base = "https://wwgs-sandbox.cloud.akeneo.com/api/rest/v1/product-models/"
auth_url = "https://wwgs-sandbox.cloud.akeneo.com/api/oauth/v1/token"
client_username = os.getenv("WWGS_Akeneo_Username")
client_password = os.getenv("WWGS_Akeneo_Password")
client_auth = os.getenv("WWGS_Base_64")


def get_auth():
    auth_headers = {"Content-Type": "application/json", "Authorization": client_auth}
    auth_body = {"grant_type": "password", "username": client_username, "password": client_password}
    auth_response = requests.post(url=auth_url, headers=auth_headers, json=auth_body)
    if not auth_response.ok:
        print(auth_response.status_code)
        exit(1)
    access_token = auth_response.json()["access_token"]
    return access_token


def get_variation(parent, access_token, thread_id):
    var_url = f"{url_base}{parent}"
    var_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {access_token}"}
    var_response = requests.get(url=var_url, headers=var_headers)
    response_json = var_response.json()
    if var_response.ok:
        return response_json["family_variant"]
    else:
        print(f"Thread ID: {thread_id} Could not return variation information for parent {parent}")
        print(f"Thread ID: {thread_id} Status Code: {var_response.status_code} Reason: {var_response.reason}")


def process_update(update, access_token):
    thread_id = threading.get_ident()
    parent = update[0]
    pfid = update[1].strip()
    print(f"Thread ID: {thread_id}. Processing parent:{parent} with PFID:{pfid}")
    url = f"{url_base}{parent}"
    headers = {"Content-Type": "application/json", "Authorization": f"Bearer {access_token}"}
    body = {"code": pfid, "family_variant": get_variation(parent, access_token, thread_id)}
    patch_response = requests.patch(url=url, headers=headers, json=body)
    if patch_response.ok:
        print(f"Thread ID: {thread_id} Status Code: {patch_response.status_code}")
    else:
        print(f"Thread ID: {thread_id} Status Code: {patch_response.status_code} Reason: {patch_response.reason}")


def main():
    print("Starting product update process...")
    access_token = get_auth()
    process_update_threaded = partial(process_update, access_token=access_token)
    with open(file, "r") as file_input:
        updates = [line.split("|") for line in file_input]
    with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
        executor.map(process_update_threaded, updates)
    print("Product update process has finished.")


if __name__ == '__main__':
    main()
