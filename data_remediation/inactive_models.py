# generate a report of all the models in Akeneo that have no SKUS
import json
import time
from datetime import datetime

import requests

from config import *

model_counter = 0


def process_all_models():
    url = f"{akeneo_url_base}/rest/v1/product-models?pagination_type=search_after&limit=100"

    models_links = process_page_of_models(url)
    while "next" in models_links:
        models_links = process_page_of_models(models_links["next"]["href"])


def process_page_of_models(url):
    logger.info("Processing a page of models")
    models_response = call_service(url, akeneo_headers, "GET")
    if "_embedded" in models_response and "items" in models_response["_embedded"]:
        models = models_response["_embedded"]["items"]
        for model_data in models:
            try:
                process_model(model_data)
            except ChildProcessError as ce:
                logger.error(ce)

    return models_response["_links"]


def process_model(model_data):
    model_code = model_data["code"]
    global model_counter
    model_counter += 1
    logger.info(f"{model_counter}|Processing Akeneo Model {model_code}")

    child_skus = get_child_skus(model_code)

    if child_skus:
        return

    logger.info(f"{model_code} is Inactive")
    inactive_models.append(model_code)


def get_child_skus(model_code):
    search_query = {
        "parent": [
            {"operator": "=",
             "value": model_code}
        ]
    }
    url = f"{akeneo_url_base}/rest/v1/products?search={json.dumps(search_query)}&limit=100"

    response = call_service(url, akeneo_headers, "GET")
    if "_embedded" in response and "items" in response["_embedded"]:
        return response["_embedded"]["items"]
    else:
        logger.error(f"Unable to retrieve child SKUs of {model_code}")
        return None


def write_report():
    logger.info(f"Writing report with {len(inactive_models)} Inactive Models")
    filename = f"inactive_models_report-{akeneo_env}.txt"
    with open(filename, "w") as file:
        for inactive_model in inactive_models:
            file.write(f"{inactive_model}\n")


def delete_models():
    logger.info(f"Deleting all {len(inactive_models)} inactive Models")
    for inactive_model in inactive_models:
        logger.info(f"Deleting Model {inactive_model}")
        url = f"{akeneo_url_base}/rest/v1/product-models/{inactive_model}"
        call_service(url, akeneo_headers, "DELETE")


def setup_logger(filename):
    timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    log_format = "%(asctime)s|%(levelname)s|%(threadName)s|%(message)s"
    global log_filename
    log_filename = f"./logs/{filename}_{timestamp}.log"

    logging.basicConfig(format=log_format)
    formatter = logging.Formatter(log_format)
    logger.setLevel(logging.INFO)

    info_log_channel = logging.FileHandler(log_filename)
    info_log_channel.setLevel(logging.INFO)
    info_log_channel.setFormatter(formatter)
    logger.addHandler(info_log_channel)


def call_service(url, headers, method, data=None, params=None, attempt=1, handle_404=False):
    response = requests.request(method=method, url=url, headers=headers, json=data, params=params)
    if response.ok:
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return {}
    elif 400 <= response.status_code < 500 and response.status_code != 429:  # 429 is a rate limit response
        if response.status_code == 401:  # 401 is an invalid authentication response
            set_auth()
            return call_service(url, akeneo_headers, method, data, params, attempt + 1)
        elif handle_404 and response.status_code == 404:
            return None
        else:
            raise ChildProcessError(
                f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
    else:
        logger.error(f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
        if attempt < number_of_attempts:
            logger.info(f"Attempt {attempt}. Waiting {retry_wait} seconds and then retrying: {url}")
            time.sleep(retry_wait)
            return call_service(url, headers, method, data, params, attempt + 1)
        else:
            raise ChildProcessError(
                f"Unable to get a successful response after {number_of_attempts} attempts on: {url}")


def set_auth():
    logger.info("Authenticating with Akeneo")

    global akeneo_token
    global akeneo_headers

    auth_response = call_service(auth_url, auth_headers, "POST", data=auth_body)
    akeneo_token = auth_response["access_token"]
    akeneo_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {akeneo_token}"}


def main():
    setup_logger(f"inactive_model_reporter_{akeneo_env}")
    logger.info(INFOVERITY)
    logger.info(f"Begin Inactive Model Reporter in {akeneo_env}")
    set_auth()
    process_all_models()
    write_report()
    delete_models()
    logger.info("Inactive Model Reporter Complete")


if __name__ == '__main__':
    main()
