import os
import logging


akeneo_env = "Sandbox"
if akeneo_env == "Sandbox":
    akeneo_url_base = "https://wwgs-sandbox.cloud.akeneo.com/api"
    akeneo_username = os.getenv("WWGS_Akeneo_Username")
    akeneo_password = os.getenv("WWGS_Akeneo_Password")
    akeneo_auth = os.getenv("WWGS_Base_64")
elif akeneo_env == "Production":
    akeneo_url_base = "https://wwgs.cloud.akeneo.com/api"
    akeneo_username = os.getenv("WWGS_Akeneo_Username_PROD")
    akeneo_password = os.getenv("WWGS_Akeneo_Password_PROD")
    akeneo_auth = os.getenv("WWGS_Base_64_PROD")
else:
    raise ChildProcessError("Akeneo Environment not set correctly in config. Valid values are: Sandbox, Production")

auth_url = f"{akeneo_url_base}/oauth/v1/token"
auth_headers = {"Content-Type": "application/json", "Authorization": akeneo_auth}
auth_body = {"grant_type": "password", "username": akeneo_username, "password": akeneo_password}

logger = logging.getLogger()
log_filename = ""

akeneo_token = None
akeneo_headers = None

inactive_models = []

INFOVERITY = """
 _____  ____  _____  ________    ___   ____   ____  ________  _______     _____  _________  ____  ____  
|_   _||_   \|_   _||_   __  | .'   `.|_  _| |_  _||_   __  ||_   __ \   |_   _||  _   _  ||_  _||_  _| 
  | |    |   \ | |    | |_ \_|/  .-.  \ \ \   / /    | |_ \_|  | |__) |    | |  |_/ | | \_|  \ \  / /   
  | |    | |\ \| |    |  _|   | |   | |  \ \ / /     |  _| _   |  __ /     | |      | |       \ \/ /    
 _| |_  _| |_\   |_  _| |_    \  `-'  /   \ ' /     _| |__/ | _| |  \ \_  _| |_    _| |_      _|  |_    
|_____||_____|\____||_____|    `.___.'     \_/     |________||____| |___||_____|  |_____|    |______|   
                                                                                                        """

number_of_attempts = 5
retry_wait = 30  # Time in seconds to wait between retries
