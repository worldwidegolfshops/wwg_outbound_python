# This will loop through all Models in Akeneo
#   Loop through the SKUs on each Model
#       Add up the values of 'SIMPLE_COLOR'
#   Then apply that aggregated color value to the Multi-Value 'COLORS' on the Model

import json
import time
from datetime import datetime

import requests

from config import *


model_counter = 0
bad_na_models = []


def process_all_models():
    url = f"{akeneo_url_base}/rest/v1/product-models?pagination_type=search_after&limit=100"

    models_links = process_page_of_models(url)
    while "next" in models_links:
        models_links = process_page_of_models(models_links["next"]["href"])


def process_page_of_models(url):
    logger.info("Processing a page of models")
    models_response = call_service(url, akeneo_headers, "GET")
    if "_embedded" in models_response and "items" in models_response["_embedded"]:
        models = models_response["_embedded"]["items"]
        for model_data in models:
            try:
                process_model(model_data)
            except ChildProcessError as ce:
                logger.error(ce)

    return models_response["_links"]


def process_model(model_data):
    model_code = model_data["code"]
    global model_counter
    model_counter += 1
    logger.info(f"{model_counter}|Processing Akeneo Model {model_code}")
    family_variant = model_data["family_variant"]
    simple_color_aggregation = set()
    child_skus = get_child_skus(model_code)
    if child_skus:
        for child_sku in child_skus:
            process_sku(child_sku, simple_color_aggregation)
    else:
        logger.info("Model has no Child SKUs")

    update_model(family_variant, simple_color_aggregation, model_code)


def get_child_skus(model_code):
    child_skus = []

    search_query = {
        "parent": [
            {"operator": "=",
             "value": model_code}
        ]
    }
    url = f"{akeneo_url_base}/rest/v1/products?search={json.dumps(search_query)}&limit=100"

    response = call_service(url, akeneo_headers, "GET")
    for sibling_sku in response["_embedded"]["items"]:
        child_skus.append(sibling_sku)

    while "next" in response["_links"]:
        response = call_service(response["_links"]["next"]["href"], akeneo_headers, "GET")
        for sibling_sku in response["_embedded"]["items"]:
            child_skus.append(sibling_sku)

    return child_skus


def process_sku(sku_data, simple_color_aggregation):
    if "SIMPLE_COLOR" in sku_data["values"]:
        for simple_color in sku_data["values"]["SIMPLE_COLOR"]:
            for color_value in simple_color["data"]:
                simple_color_aggregation.add(color_value)


def update_model(family_variant, simple_color_aggregation, model_code):
    logger.info(f"Updating model with: {simple_color_aggregation}")

    if "N_A" in simple_color_aggregation:
        if len(simple_color_aggregation) > 1:
            logger.warning(f"Model {model_code} has N_A and other values")
            bad_na_models.append(model_code)
            return
        else:
            simple_color_aggregation = set()

    url = f"{akeneo_url_base}/rest/v1/product-models/{model_code}"
    colors_value = [{"locale": None, "scope": None, "data": list(simple_color_aggregation)}]
    model_data = {"code": model_code,
                  "family_variant": family_variant,
                  "values": {"COLORS": colors_value}}

    call_service(url, akeneo_headers, "PATCH", data=model_data)


def setup_logger(filename):
    timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    log_format = "%(asctime)s|%(levelname)s|%(threadName)s|%(message)s"
    global log_filename
    log_filename = f"./logs/{filename}_{timestamp}.log"

    logging.basicConfig(format=log_format)
    formatter = logging.Formatter(log_format)
    logger.setLevel(logging.INFO)

    info_log_channel = logging.FileHandler(log_filename)
    info_log_channel.setLevel(logging.INFO)
    info_log_channel.setFormatter(formatter)
    logger.addHandler(info_log_channel)


def call_service(url, headers, method, data=None, params=None, attempt=1, handle_404=False):
    response = requests.request(method=method, url=url, headers=headers, json=data, params=params)
    if response.ok:
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return {}
    elif 400 <= response.status_code < 500 and response.status_code != 429:  # 429 is a rate limit response
        if response.status_code == 401:  # 401 is an invalid authentication response
            set_auth()
            return call_service(url, akeneo_headers, method, data, params, attempt + 1)
        elif handle_404 and response.status_code == 404:
            return None
        else:
            raise ChildProcessError(
                f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
    else:
        logger.error(f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
        if attempt < number_of_attempts:
            logger.info(f"Attempt {attempt}. Waiting {retry_wait} seconds and then retrying: {url}")
            time.sleep(retry_wait)
            return call_service(url, headers, method, data, params, attempt + 1)
        else:
            raise ChildProcessError(
                f"Unable to get a successful response after {number_of_attempts} attempts on: {url}")


def set_auth():
    logger.info("Authenticating with Akeneo")

    global akeneo_token
    global akeneo_headers

    auth_response = call_service(auth_url, auth_headers, "POST", data=auth_body)
    akeneo_token = auth_response["access_token"]
    akeneo_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {akeneo_token}"}


def main():
    setup_logger("color_aggregator")
    logger.info(INFOVERITY)
    logger.info(f"Begin Color Aggregation in {akeneo_env}")
    set_auth()
    process_all_models()
    logger.info(f"All Product Models that have N_A and other values: {bad_na_models}")
    logger.info("Color Aggregation Complete")


if __name__ == '__main__':
    main()
