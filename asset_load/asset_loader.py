# Asset Automation - Load Process
#
# 1. read through folder 'B' recursively
#
# 2. parse images based on their filename into the following 6 groups
# 	- Product Model Images - Alternate  (Bucket_Hats_Sun_Hats_100009247-a.jpg)
# 	- Product Model Images - Main 		(Bucket_Hats_Sun_Hats_100009247.jpg)
# 	- SKU Images - Colorway - Alternate (Athletic_Spiked_Golf_Shoes_300002373-color_whiteacidyellowblueoxide-d.jpg)
# 	- SKU Images - Colorway - Main		(Athletic_Spiked_Golf_Shoes_300002373-color_whiteacidyellowblueoxide.jpg)
# 	- SKU Images - Team - Alternate		(Gloves_10132285-team_PittsburghSteelers-c.jpg)
# 	- SKU Images - Team - Main			(Gloves_10132285-team_PittsburghSteelers.jpg)
#
# 3. create excel file
# 	- "code" (filename, replace - with _, remove file extension)
# 	- "assetFamilyIdentifier" (from akeneo)
# 	- "media" (generic filename)
#
# 	1. need to move the files from B folder to parsed folders (zip files)
# 	2. generate the excel file
# 	3. add excel file with media files and zip together
#
import glob
import os.path
import shutil
import xlsxwriter
import re


# Package up after this threshold: 500MB (actually 499MB to be safe)
file_size_threshold = 499000000
# Global to track current package file size
file_size_sum = 0
# Number to append to each archive iteratively
package_number = 1

# list out asset family codes for each asset family
pmi_alt_id = 'PRODUCT_MODEL_IMAGES_ALTERNATES'
pmi_main_id = 'PRODUCT_MODEL_IMAGES_MAIN'
sku_alt_id = 'SKU_IMAGES_COLORWAY_ALTERNATES'
sku_main_id = 'SKU_IMAGES_COLORWAY_MAIN'
sku_team_id = 'SKU_IMAGES_TEAM_MAIN'
team_alt_id = 'SKU_IMAGES_TEAM_ALTERNATE'

# set the file location targets for file transfers
output_root = "C:/Users/DSpain/OneDrive - Infoverity, Inc/Documents/Projects!/WWG CO9/ProductionAssetLoad"
pmi_alt_target = f'{output_root}/ProductModelImagesAlternate'
pmi_main_target = f'{output_root}/ProductModelImagesMain'
sku_alt_target = f'{output_root}/SkuColorwayAlternate'
sku_main_target = f'{output_root}/SkuColorwayMain'
sku_team_target = f'{output_root}/SkuTeamMain'
team_alt_target = f'{output_root}/SkuTeamAlternate'

# root_dir needs a trailing slash (i.e. /root/dir/)
image_dir = "C:/Users/DSpain/OneDrive - Infoverity, Inc/Desktop/product_images/B/"


def process_file(filename, processed_files, target, asset_family_code):
    base_name = os.path.basename(filename)
    new_location = f"{target}/{base_name}"
    print(f"Filename: {base_name} - location: {target}")
    shutil.move(filename, new_location)
    processed_files[filename] = [base_name, asset_family_code, new_location]
    return new_location


def package_files(pmi_alt, pmi_main, sku_alt, sku_main, sku_team, team_alt):
    print("Packaging current batch of files")
    # move all output folders to a new directory
    global package_number
    package_dir = f"{output_root}/image_archive_{package_number}"
    os.mkdir(package_dir)
    shutil.move(pmi_alt_target, package_dir)
    shutil.move(pmi_main_target, package_dir)
    shutil.move(sku_alt_target, package_dir)
    shutil.move(sku_main_target, package_dir)
    shutil.move(sku_team_target, package_dir)
    shutil.move(team_alt_target, package_dir)

    # make excel file in that dir
    create_workbook(package_dir, pmi_alt, pmi_main, sku_alt, sku_main, sku_team, team_alt)

    # archive that dir
    shutil.make_archive(f"image_archive_{package_number}", "zip", package_dir)
    package_number += 1

    # Recreate all the output folders
    os.mkdir(pmi_alt_target)
    os.mkdir(pmi_main_target)
    os.mkdir(sku_alt_target)
    os.mkdir(sku_main_target)
    os.mkdir(sku_team_target)
    os.mkdir(team_alt_target)


def process_files():
    print("Begin Processing Files")

    # Ensure output folders exist
    for file_path in pmi_alt_target, pmi_main_target, sku_alt_target, sku_main_target, sku_team_target, team_alt_target:
        directory = os.path.dirname(file_path)
        os.makedirs(directory, exist_ok=True)

    pmi_alt = {}
    pmi_main = {}
    sku_alt = {}
    sku_main = {}
    sku_team = {}
    team_alt = {}

    for filename in glob.iglob(image_dir + '**/**', recursive=True):
        # print(f"Checking: {filename}")

        # Product Model Images - Alternates
        if re.search(r"([a-zA-Z])_[0-9]*-[a-z]*.(jpg|JPG)$", filename):
            moved_file = process_file(filename, pmi_alt, pmi_alt_target, pmi_alt_id)

        # Product Model Images - Main
        elif re.search(r'([a-zA-Z])_[0-9]*.(jpg|JPG)$', filename):
            moved_file = process_file(filename, pmi_main, pmi_main_target, pmi_main_id)

        # SKU Images - Colorway - Alternates
        elif re.search(r'([a-zA-Z])_[0-9]*-color_[a-zA-Z]*-[a-z]*.(jpg|JPG)$', filename):
            moved_file = process_file(filename, sku_alt, sku_alt_target, sku_alt_id)

        # SKU Images - Colorway - Main
        elif re.search(r'([a-zA-Z])_[0-9]*-color_[a-zA-Z]*.(jpg|JPG)$', filename):
            moved_file = process_file(filename, sku_main, sku_main_target, sku_main_id)

        # SKU Images - Team - Main
        elif re.search(r'([a-zA-Z])_[0-9]*-team_[A-Za-z0-9_-]*.(jpg|JPG)$', filename):
            moved_file = process_file(filename, sku_team, sku_team_target, sku_team_id)

        # SKU Images - Team - Alternates
        elif re.search(r'([a-zA-Z])_[0-9]*-team_[A-Za-z0-9_-]*-[a-z]*.(jpg|JPG)$', filename):
            moved_file = process_file(filename, team_alt, team_alt_target, team_alt_id)

        else:
            print(f"=== {filename} did not match any regex")
            moved_file = None

        global file_size_sum
        if moved_file:
            try:
                file_size_sum += os.path.getsize(moved_file)
            except FileNotFoundError as e:
                print(e)
                continue
            if file_size_sum > file_size_threshold:
                package_files(pmi_alt, pmi_main, sku_alt, sku_main, sku_team, team_alt)

                file_size_sum = 0
                pmi_alt = {}
                pmi_main = {}
                sku_alt = {}
                sku_main = {}
                sku_team = {}
                team_alt = {}

    print("Files processed")


def create_workbook(package_dir, pmi_alt, pmi_main, sku_alt, sku_main, sku_team, team_alt):
    print("Begin Creating Workbook")
    # create the workbook that will be appended to and loaded into Akeneo
    workbook = xlsxwriter.Workbook(f'{package_dir}/AkeneoImageImport.xlsx')
    worksheet = workbook.add_worksheet()
    # Use the worksheet object to write in headers for the file.
    worksheet.write('A1', 'code')
    worksheet.write('B1', 'assetFamilyIdentifier')
    worksheet.write('C1', 'media')

    # Start iterating from the second cell, rows and columns start at the zero index.
    row = 1
    code_column = 0
    asset_family_id = 1
    media_column = 2

    for asset_map in pmi_alt, pmi_main, sku_alt, sku_main, sku_team, team_alt:
        for file in asset_map:
            # write operation perform placement of filename in media column
            worksheet.write(row, media_column, asset_map[file][2])

            # write operation perform placement of asset family identifier in that column
            worksheet.write(row, asset_family_id, asset_map[file][1])

            # write operation perform placement of filename in code column
            stripped = asset_map[file][0].replace("-", "_")
            clean = stripped.replace(".jpg", "")
            worksheet.write(row, code_column, clean)

            # incrementing the value of row by one
            # with each iterations.
            row += 1

    workbook.close()
    print("Workbook Created")


def main():
    print("Begin Asset Loader")
    process_files()
    print("Asset Loader complete")


if __name__ == '__main__':
    main()
