# Integrator
The Integrator is a custom integration layer, written in Python, built for Worldwide Golf to connect their Akeneo PIM with their Ecommerce platform VTEX.

Check out the [Integrator Documentation](https://docs.google.com/document/d/1A7pmGAUzbEEL--Nx0mMCSpzUDQamBitXmzHDFDwC7Rk) and the 
[Solution Design](https://docs.google.com/document/d/1K4jaHLH0PfFpdA8es92KeYoLojVTswDxQPwVsDlAQ9o) for more information.

## Authentication
Authentication is required to communicate with both Akeneo and VTEX. Credentials stored in Environment Variables are necessary to achieve proper authentication. The required values are already stored on the api server in the api user's ~/.bash_profile. If you wish to locally run the Integrator for testing purposes, you will need to set up the following environment variables.

### VTEX
Two Environment Variables are needed:

1. VTEX_APP_KEY - The Public Key used to denote an instance of connection credentials

2. VTEX_APP_TOKEN - The Private Key of this instance

### Akeneo
Three Environment Variables are needed:

1. WWGS_Akeneo_Username - A username that has full access to Akeneo

2. WWGS_Akeneo_Password - The password for this user

3. WWGS_Base_64 - A Base 64 encoding of the username and password

## Schedule
The Integrator is scheduled in the crontab to run every night. To update this schedule:

1. Connect to the api server: `ssh api@api.budgetgolf.com`

2. Log in as root: `su` and then enter the root password

3. Open the crontab: `crontab -e`

4. Find the line for the integrator. It will look something like this: `0      2       *       *       *       /bin/bash /var/www/vhosts/api.budgetgolf.com/integrator/run_integrator.sh > /var/www/vhosts/api.budgetgolf.com/integrator/logs/run_integrator.log`

5. Edit it however needed. If you need to turn off the schedule just add a `#` at the beginning of the line to comment it out

## Deploy
If you need to deploy an updated version of the Integrator.

1. Copy the updated files to
`/var/www/vhosts/api.budgetgolf.com/integrator`

Example: `scp /Users/ceft/Documents/clients/worldwide_golf/integration/integrator/* api@api.budgetgolf.com:/var/www/vhosts/api.budgetgolf.com/integrator`

2. Connect to the api server: `ssh api@api.budgetgolf.com`

3. Get all the environment variables in your current shell: `source ~/.bash_profile`

4. Change Directory to the integrator directory: `cd integrator`

5. Build the docker compose file: `docker-compose build`

After doing this, the cron job that kicks off `run_integrator.sh` will spin up the new docker compose cluster whenever it runs.

## Debug
If the Integrator ever hangs while running (the logs stop updating but the application is still running), make use of the Python faulthandler. Note: this doesn't print to the console, look into how to get it to print to a file.

1. On the api server, connect an interactive terminal to the integrator container with `docker exec -it integrator_integrator_1 /bin/bash`

2. Get the pid (process id) of the python application with `pgrep python`

3. Run the following command to send a predefined signal to the python application, which will print a stack trace (somewhere, not sure where yet): `kill -s SIGUSR1 <pid>`

## Manual Run
If you ever need to manually run the Integrator outside the regular cron schedule, do it the following way, where the working directory is `~/integrator/`

`nohup ./run_integrator.sh > ./logs/integrator.log &`

The `nohup` command ensures that even if the ssh connection is broken, the job still continues because the application ignores the hangup signal that the user sends upon disconnection.

The `&` makes the shell script run as a background process, so you can still use your terminal.