import concurrent.futures
import copy
import functools
import json
import shutil
import sys
import time
from datetime import datetime
import faulthandler
import signal
import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from config import *

import requests

faulthandler.register(signal.SIGUSR1.value)
log_filename = ""


class ItemException(Exception):
    pass


class InactiveItemException(Exception):
    pass


class Category:
    def __init__(self, code, label):
        self.code = code
        self.label = label

    @staticmethod
    def get_akeneo_categories(all_categories, url):
        categories_json = call_service(url, akeneo_headers, "GET")
        for category in categories_json["_embedded"]["items"]:
            if category["code"].endswith("_WWG"):
                all_categories.append(Category(category["code"], category["labels"]["en_US"]))

        return categories_json['_links']

    @staticmethod
    def get_all_akeneo_categories():
        all_categories = []

        url = f"{akeneo_url_base}/rest/v1/categories?limit=100"
        categories_links = Category.get_akeneo_categories(all_categories, url)

        while "next" in categories_links:
            categories_links = Category.get_akeneo_categories(all_categories, categories_links["next"]["href"])

        return all_categories

    @staticmethod
    def is_leaf(category):
        url = f'{akeneo_url_base}/rest/v1/categories?search={{"parent":[{{"operator":"=","value":"{category.code}"}}]}}'

        leaf_json = call_service(url, akeneo_headers, "GET")
        if not leaf_json["_embedded"]["items"]:
            leaf_category_code_to_name[category.code] = category.label

    @staticmethod
    def get_leaf_categories():
        logger.info("Getting all leaf categories")
        all_categories = Category.get_all_akeneo_categories()

        for category in all_categories:
            Category.is_leaf(category)

    @staticmethod
    def get_subcategories(category):
        subcategories = {}
        for subcategory in category['children']:
            subcategories[subcategory['name']] = Category.get_subcategories(subcategory)
            category_name_to_id[subcategory['name']] = subcategory['id']

        return subcategories

    @staticmethod
    def get_vtex_categories() -> dict:
        category_tree = {}
        url = f"{vtex_url_base}/catalog_system/pvt/category/tree/10"
        category_json = call_service(url, vtex_headers, "GET")
        for root_category in category_json:
            category_tree[root_category['name']] = Category.get_subcategories(root_category)
            category_name_to_id[root_category['name']] = root_category['id']

        return category_name_to_id


class Specification:
    def __init__(self, association_id, value):
        self.association_id = association_id
        self.value = value


class Attribute:
    def __init__(self, code, label, attribute_id, data=None, attribute_type=None, item_type=None):
        self.code = code
        self.label = label
        self.id = attribute_id
        self.data = data
        self.attribute_type = attribute_type
        self.item_type = item_type  # SKU or PRODUCT

    def __eq__(self, other):
        return self.code == other.code and self.label == other.label

    def __hash__(self):
        return hash(f"{self.code}{self.label}")

    @staticmethod
    def set_akeneo_asset_attributes():
        url = f"{akeneo_url_base}/rest/v1/asset-families"
        response = call_service(url, akeneo_headers, "GET")
        for asset_family in response["_embedded"]["items"]:
            link_rules = asset_family["product_link_rules"]
            if len(link_rules) > 0 and "assign_assets_to" in link_rules[0]:
                attribute: str = link_rules[0]["assign_assets_to"][0]["attribute"]
                asset_attribute_to_family_code[attribute] = asset_family["code"]
                if attribute.endswith("_HERO"):
                    akeneo_asset_attributes["HERO"].append(attribute)
                elif attribute.endswith("_MAIN"):
                    akeneo_asset_attributes["MAIN"].append(attribute)
                elif attribute.endswith("_ALTERNATES"):
                    akeneo_asset_attributes["ALTERNATE"].append(attribute)

    @staticmethod
    def get_all_akeneo_attribute_options(akeneo_attributes):
        logger.info("Getting all Akeneo Attribute Options")
        for attribute in akeneo_attributes:
            if attribute.attribute_type not in ["pim_catalog_simpleselect", "pim_catalog_multiselect"]:
                continue

            akeneo_attribute_options[attribute.code] = {}
            url = f"{akeneo_url_base}/rest/v1/attributes/{attribute.code}/options?limit=100"
            response_links = Attribute.get_attributes_options(url, attribute.code)

            while "next" in response_links:
                response_links = Attribute.get_attributes_options(response_links["next"]["href"], attribute.code)

    @staticmethod
    def get_attributes_options(url, attribute_code):
        attributes_options = call_service(url, akeneo_headers, "GET")

        for attribute_option in attributes_options["_embedded"]["items"]:
            if "en_US" in attribute_option["labels"]:
                akeneo_attribute_options[attribute_code][attribute_option["code"]] = attribute_option["labels"]["en_US"]
            else:
                logger.warning(f"Attribute option has no English label: {attribute_option}")

        return attributes_options["_links"]

    @staticmethod
    def get_akeneo_attributes():
        akeneo_attribute_codes = set()
        akeneo_attributes = set()

        families_url = f"{akeneo_url_base}/rest/v1/families?limit=100"
        families_json = call_service(families_url, akeneo_headers, "GET")
        families = families_json["_embedded"]["items"]
        for family in families:
            for attribute_code in family["attributes"]:
                akeneo_attribute_codes.add(attribute_code)

        attribute_partial = functools.partial(Attribute.get_akeneo_attribute, akeneo_attributes=akeneo_attributes)
        with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
            executor.map(attribute_partial, akeneo_attribute_codes)

        for attribute_code in additional_attributes_to_push:
            attributes_to_push.add(attribute_code)

        # Make a separate set for Attributes that need options, but aren't going to Specs
        akeneo_attributes_for_options = copy.deepcopy(akeneo_attributes)
        for attribute_code in additional_attributes_with_options:
            Attribute.get_akeneo_attribute(attribute_code, akeneo_attributes_for_options, True)
        Attribute.get_all_akeneo_attribute_options(akeneo_attributes_for_options)

        return akeneo_attributes

    @staticmethod
    def get_akeneo_attribute(attribute_code, akeneo_attributes, additional_attributes=False):
        attribute_url = f"{akeneo_url_base}/rest/v1/attributes/{attribute_code}"
        attribute_properties = call_service(attribute_url, akeneo_headers, "GET")

        if 'sw_KE' in attribute_properties['labels']:
            akeneo_attributes.add(
                Attribute(attribute_properties['code'], attribute_properties['labels']['sw_KE'], None,
                          attribute_type=attribute_properties["type"], item_type="SKU"))
            attributes_to_push.add(attribute_properties['code'])
        elif 'ps_AF' in attribute_properties['labels']:
            akeneo_attributes.add(
                Attribute(attribute_properties['code'], attribute_properties['labels']['ps_AF'], None,
                          attribute_type=attribute_properties["type"], item_type="PRODUCT"))
            attributes_to_push.add(attribute_properties['code'])
        elif attribute_code in additional_attributes_with_options and additional_attributes:
            akeneo_attributes.add(
                Attribute(attribute_properties['code'], attribute_properties['labels']['en_US'], None,
                          attribute_type=attribute_properties["type"], item_type="NONE"))

    @staticmethod
    def get_field_id_descriptions(vtex_params, vtex_specifications_field_ids):
        vtex_specifications = set()

        field_partial = functools.partial(Attribute.get_field_id_description, vtex_specifications=vtex_specifications,
                                          vtex_params=vtex_params)
        with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
            executor.map(field_partial, vtex_specifications_field_ids)

        return vtex_specifications

    @staticmethod
    def get_field_id_description(field_id, vtex_specifications, vtex_params):
        field_id_url = f"{vtex_url_base}/catalog_system/pub/specification/fieldGet/{field_id}"
        field_json = call_service(field_id_url, vtex_headers, "GET", params=vtex_params)

        attribute_code = field_json["Description"]
        if field_json["IsStockKeepingUnit"]:
            item_type = "SKU"
        else:
            item_type = "PRODUCT"
        attribute = Attribute(attribute_code, field_json["Name"], field_json["FieldId"],
                              attribute_type=field_json["FieldTypeId"], item_type=item_type)
        vtex_specifications.add(attribute)
        attribute_code_to_object[attribute_code] = attribute

    @staticmethod
    def get_vtex_field_values(field_id):
        url = f"{vtex_url_base}/catalog_system/pub/specification/fieldvalue/{field_id}"
        response_json = call_service(url, vtex_headers, "GET")

        value_to_id = {}
        for field_value in response_json:
            value_to_id[field_value["Value"]] = field_value["FieldValueId"]
        field_to_value_to_id[field_id] = value_to_id

    @staticmethod
    def get_vtex_specifications():
        vtex_field_ids = set()
        specifications_url = f"{vtex_url_base}/catalog_system/pub/specification/field/listTreeByCategoryId/0"
        vtex_params = {"pagesize": "1000"}

        # get all of the category root specifications in VTEX
        fields_json = call_service(specifications_url, vtex_headers, "GET", None, vtex_params, logger)
        for specification in fields_json:
            if specification["IsActive"]:
                vtex_field_ids.add(specification["FieldId"])

        # from these specs, get their field values
        with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
            executor.map(Attribute.get_vtex_field_values, vtex_field_ids)

        # build the attribute code to object map and return the set of vtex specs
        return Attribute.get_field_id_descriptions(vtex_params, vtex_field_ids)

    @staticmethod
    def process_specifications_update(vtex_removals, vtex_additions, vtex_updates):
        for removal in vtex_removals:
            removal_url = f"{vtex_url_base}/catalog/pvt/specification/{attribute_code_to_object[removal.code].id}"
            data = call_service(removal_url, vtex_headers, "GET")
            data["IsActive"] = False
            data["Name"] = f"{data['Name']}-OLD"

            logger.info(f"Inactivating Attribute: {data}")
            call_service(removal_url, vtex_headers, "PUT", data)

        for addition in vtex_additions:
            logger.info(f"Adding Attribute: {vars(addition)}")

            addition_url = f"{vtex_url_base}/catalog/pvt/specification"
            data = copy.deepcopy(VTEX_SPEC_BODY)
            data["Name"] = addition.label
            data["Description"] = addition.code

            if addition.item_type == "SKU":
                data["IsStockKeepingUnit"] = True
                data["FieldTypeId"] = attribute_to_specification_type_sku[addition.attribute_type]
            else:
                data["IsStockKeepingUnit"] = False
                data["FieldTypeId"] = attribute_to_specification_type_product[addition.attribute_type]

            call_service(addition_url, vtex_headers, "POST", data)

        for update in vtex_updates:
            update_url = f"{vtex_url_base}/catalog/pvt/specification/{attribute_code_to_object[update.code].id}"
            data = call_service(update_url, vtex_headers, "GET")
            data["Name"] = update.label

            logger.info(f"Updating attribute: {data}")
            call_service(update_url, vtex_headers, "PUT", data)

    @staticmethod
    def build_system_attribute_data():
        logger.info("Begin processing Attributes")
        Attribute.set_akeneo_asset_attributes()
        akeneo_attributes = Attribute.get_akeneo_attributes()
        vtex_specifications = Attribute.get_vtex_specifications()

        if akeneo_attributes == vtex_specifications:
            logger.info("Akeneo Attributes are synced with VTEX Specifications")
        else:
            logger.info("Akeneo Attributes are not synced with VTEX Specifications. Syncing")

            vtex_removals, vtex_additions, vtex_updates = Attribute.determine_removals_additions_updates(
                akeneo_attributes, vtex_specifications)

            Attribute.process_specifications_update(vtex_removals, vtex_additions, vtex_updates)

    @staticmethod
    def determine_removals_additions_updates(akeneo_attributes, vtex_specifications):
        removals = vtex_specifications.difference(akeneo_attributes)
        additions = akeneo_attributes.difference(vtex_specifications)

        updates = set()
        removes = set()

        addition_codes = set()
        for addition in additions:
            addition_codes.add(addition.code)

        removal_codes = set()
        for removal in removals:
            removal_codes.add(removal.code)

        update_codes = addition_codes.intersection(removal_codes)
        for addition in additions:
            if addition.code in update_codes:
                updates.add(addition)

        for removal in removals:
            if removal.code in update_codes:
                removes.add(removal)

        additions -= updates
        removals -= removes

        return removals, additions, updates

    def sync(self, item):
        field_values: list = Attribute.get_field_values(Attribute.get_data_value(self.data))

        if not field_values:
            logger.warning("There were no field values found. Skipping")
            return

        if self.code in akeneo_attribute_options:
            # Switch Attribute Option Code for Label
            field_label_values = []
            for field_value in field_values:
                if field_value in akeneo_attribute_options[self.code]:
                    field_label_values.append(akeneo_attribute_options[self.code][field_value])
                else:
                    raise ItemException(f"There is no Attribute Option Label for Attribute: {self.code}."
                                        f"Attribute Option: {field_value}")
            field_values = field_label_values

        if self.item_type == "SKU":
            self.sync_sku_specification(item, field_values[0])
        else:
            self.sync_product_specification(item, field_values)

    def sync_product_specification(self, item, field_values: list):
        if delete_attribute_value_1 in field_values or delete_attribute_value_2 in field_values \
                or delete_attribute_value_3 in field_values:
            if self.id in item.vtex_specs:
                for spec in item.vtex_specs[self.id]:
                    url = f"{vtex_url_base}/catalog/pvt/product/{item.vtex_id}/specification/{spec.association_id}"
                    call_service(url, vtex_headers, "DELETE")
        else:
            if self.attribute_type in [1, 2, 3]:
                self.process_text_spec(item, field_values)
            else:
                self.process_select_spec(item, field_values)

    def process_text_spec(self, item, field_values):
        # This is a text/number field and can be updated directly
        if self.id in item.vtex_specs:
            # This field is already in VTEX
            url = f"{vtex_url_base}/catalog_system/pvt/products/{item.vtex_id}/specification"
            body = [{"id": self.id, "value": field_values}]  # the value must be a list here
            call_service(url, vtex_headers, "POST", body)
        else:
            # This field is not in VTEX yet
            url = f"{vtex_url_base}/catalog/pvt/product/{item.vtex_id}/specification"
            for field_value in field_values:
                body = {"FieldId": self.id, "Text": field_value}  # the value can't be a list here
                call_service(url, vtex_headers, "POST", body)

    def process_select_spec(self, item, field_values):
        # This is a select field, and we must fetch the field value ids
        specs_to_delete, field_values_to_add = self.get_spec_differences(item, field_values)

        for spec in specs_to_delete:
            url = f"{vtex_url_base}/catalog/pvt/product/{item.vtex_id}/specification/{spec.association_id}"
            call_service(url, vtex_headers, "DELETE")

        for field_value in field_values_to_add:
            field_value_id = self.get_field_value_id(field_value)
            url = f"{vtex_url_base}/catalog/pvt/product/{item.vtex_id}/specification"
            body = {"FieldId": self.id, "FieldValueId": field_value_id}
            call_service(url, vtex_headers, "POST", body)

    def sync_sku_specification(self, item, field_value):
        if field_value == delete_attribute_value_1 or field_value == delete_attribute_value_2 \
                or field_value == delete_attribute_value_3:
            if self.id in item.vtex_specs:
                spec = item.vtex_specs[self.id]
                url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{item.vtex_id}/specification/{spec.association_id}"
                call_service(url, vtex_headers, "DELETE")

        else:
            if self.id in item.vtex_specs and item.vtex_specs[self.id].value == field_value:
                # The same value is in VTEX already. Skipping
                return

            field_value_id = self.get_field_value_id(field_value)
            url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{item.vtex_id}/specification"
            if self.id in item.vtex_specs:
                method = "PUT"
                association_id = item.vtex_specs[self.id].association_id
                body = {"Id": association_id, "FieldId": self.id, "FieldValueId": field_value_id}
            else:
                method = "POST"
                body = {"FieldId": self.id, "FieldValueId": field_value_id}

            call_service(url, vtex_headers, method, body)

    def get_spec_differences(self, item, field_values):
        specs_to_delete = []  # those is VTEX but not Akeneo
        field_values_to_add = []  # those in Akeneo but not VTEX

        for field_value in field_values:
            if not self.attribute_value_in_vtex(item, field_value):
                field_values_to_add.append(field_value)

        if self.id in item.vtex_specs:
            for spec in item.vtex_specs[self.id]:
                if not self.spec_value_in_akeneo(spec, field_values):
                    specs_to_delete.append(spec)

        return specs_to_delete, field_values_to_add

    def attribute_value_in_vtex(self, item, field_value):
        if self.id in item.vtex_specs:
            for spec in item.vtex_specs[self.id]:
                if field_value == spec.value:
                    return True

        return False

    @staticmethod
    def spec_value_in_akeneo(spec, field_values):
        for field_value in field_values:
            if spec.value == field_value:
                return True

        return False

    @staticmethod
    def get_field_values(data):
        # This always returns a list of values, even if there is only one value
        if isinstance(data, list):
            if isinstance(data[0], dict):
                logger.warning("Dictionary object in data. Skipping for now.")
                logger.warning(data[0])
                return None
            return data
        elif isinstance(data, dict):
            logger.warning("Dictionary object in data. Skipping for now.")
            logger.warning(data)
            return None
        elif isinstance(data, bool):
            if data:
                return ["true"]
            else:
                return ["false"]
        elif isinstance(data, str) or isinstance(data, int):
            return [data]
        else:
            logger.warning(f"Unknown data type. Skipping. Data: {data}")
            return None

    def get_field_value_id(self, value):
        if value and value in field_to_value_to_id[self.id]:
            field_value_id = field_to_value_to_id[self.id][value]
        else:
            # This field value doesn't exist in VTEX
            data = copy.deepcopy(VTEX_VALUE_BODY)
            data["FieldId"] = self.id
            data["Name"] = value
            url = f"{vtex_url_base}/catalog_system/pvt/specification/fieldValue"
            response = call_service(url, vtex_headers, "POST", data)
            field_value_id = response["FieldValueId"]
            field_to_value_to_id[self.id][value] = field_value_id

        return field_value_id

    @staticmethod
    def get_data_value(data, scope="WORLD_WIDE_GOLF"):
        data_index = Attribute.get_data_index(data, scope)
        if data_index >= 0:
            return data[data_index]["data"]
        else:
            return None

    @staticmethod
    def get_data_index(data, scope="WORLD_WIDE_GOLF"):
        if len(data) == 1:
            return 0
        for count, datum in enumerate(data):
            if datum["scope"] == scope:
                return count

        return -1


class SKU:
    total_count = 0
    success_count = 0
    error_count = 0
    not_processed = set()
    page_number = 1

    def __init__(self, data):
        self.akeneo_id = data["identifier"]
        if self.akeneo_id in skus_processed:
            raise InactiveItemException
        if test_sku_ids and self.akeneo_id not in test_sku_ids:
            raise InactiveItemException
        self.attributes = data["values"]
        self.enabled = data["enabled"]
        self.is_active = self.sku_is_active()
        self.vtex_sku = self.get_vtex_sku(self.akeneo_id)
        self.vtex_id = self.vtex_sku["Id"]
        self.vtex_parent_refid = None
        self.vtex_product = None
        self.vtex_product_id = None
        self.vtex_product_needs_update = False
        self.vtex_sibling_sku_files = None  # Map of VTEX SKU ID to SKU Files' Labels
        self.categories = data["categories"]
        self.data = data
        self.akeneo_sibling_skus = set()
        self.vtex_specs = None  # Map of FieldId to Specification Object
        self.vtex_assets = None
        self.vtex_sku_context = None
        self.product = None

    def sku_is_active(self):
        is_active = False

        if "WWG_WEB_READY" in self.attributes:
            web_ready_sku = Attribute.get_data_value(self.attributes["WWG_WEB_READY"])
        else:
            web_ready_sku = None

        if "WWG_WEB_READY_PRODUCT_MODEL" in self.attributes:
            web_ready_model = Attribute.get_data_value(self.attributes["WWG_WEB_READY_PRODUCT_MODEL"])
        else:
            web_ready_model = None

        if "WWG_WEB_READY_ASSETS" in self.attributes:
            web_ready_assets = Attribute.get_data_value(self.attributes["WWG_WEB_READY_ASSETS"])
        else:
            web_ready_assets = None

        if "PRICE_LOGIC" in self.attributes:
            price_logic = Attribute.get_data_value(self.attributes["PRICE_LOGIC"])
        else:
            price_logic = None

        if "QTY158" in self.attributes:
            inventory = Attribute.get_data_value(self.attributes["QTY158"])
        else:
            inventory = None

        if self.enabled and web_ready_sku and web_ready_model and web_ready_assets:
            if price_logic in ('96', '97'):
                if inventory and inventory > 0:
                    is_active = True
            else:
                is_active = True

        return is_active

    def action(self):
        sku_body = self.build_vtex_sku_body()
        if self.vtex_id:
            # This SKU exists in VTEX
            url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}"
            self.vtex_sku = call_service(url, vtex_headers, "PUT", sku_body)
        else:
            # This SKU does not exist in VTEX
            url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit"
            self.vtex_sku = call_service(url, vtex_headers, "POST", sku_body)

        self.vtex_id = self.vtex_sku["Id"]

    def build_vtex_sku_body(self):
        if self.vtex_id:
            # This SKU exists in VTEX
            body = self.vtex_sku
            body["ActivateIfPossible"] = False
        else:
            # This SKU does not exist in VTEX
            body = copy.deepcopy(VTEX_SKU_BODY)
            body["Id"] = self.akeneo_id
            body["RefId"] = self.akeneo_id
            body["Name"] = self.akeneo_id

        self.vtex_product = self.get_vtex_product()
        self.vtex_product_id = self.vtex_product["Id"]
        body["ProductId"] = self.vtex_product_id

        if "ITEM_SID" in self.attributes:
            body["ManufacturerCode"] = Attribute.get_data_value(self.attributes["ITEM_SID"])

        # UDF1 is Launch Date and UDF5 is Presale Date
        if "UDF1" in self.attributes and "UDF5" in self.attributes:
            body["EstimatedDateArrival"] = self.get_date("UDF1")

        return body

    def get_date(self, attribute_code):
        presale_date_raw = Attribute.get_data_value(self.attributes[attribute_code])
        presale_date = datetime.strptime(presale_date_raw, "%Y-%m-%dT%H:%M:%S+00:00")
        if presale_date.year == 2012 and presale_date.month == 12 and presale_date.day == 12:
            return None
        else:
            return presale_date_raw

    def get_earliest_date(self, attribute_code):
        if not self.get_date(attribute_code):
            return None

        earliest_date = None

        for sibling_sku in self.akeneo_sibling_skus:
            if attribute_code in sibling_sku["values"]:
                date_raw = Attribute.get_data_value(sibling_sku["values"][attribute_code])
                date = datetime.strptime(date_raw, "%Y-%m-%dT%H:%M:%S+00:00")
                if earliest_date:
                    if date < earliest_date:
                        earliest_date = date
                else:
                    earliest_date = date

        return datetime.strftime(earliest_date, "%Y-%m-%dT%H:%M:%S+00:00")

    def get_vtex_product(self):
        if self.data["parent"]:
            # This SKU has a parent Model
            self.vtex_parent_refid = self.get_top_level(self.data["parent"])
            standalone_sku = False
        else:
            # This is a standalone SKU
            raise InactiveItemException

        vtex_product = Product.get_vtex_product(self.vtex_parent_refid)

        if not vtex_product["Id"]:
            # This product does not exist in VTEX
            raise ItemException(f"Parent Akeneo Model {self.vtex_parent_refid} doesn't exist in VTEX."
                                f" Skipping processing of this SKU until its parent exists in VTEX.")

        return vtex_product

    @staticmethod
    def get_top_level(parent_id):
        url = f"{akeneo_url_base}/rest/v1/product-models/{parent_id}"
        response = call_service(url, akeneo_headers, "GET")
        # This logic will ignore all 'middle' tier models, as we only want the top and bottom levels when there's 3.
        if response["parent"]:
            return response["parent"]
        else:
            return response["code"]

    def process_attributes(self):
        self.vtex_specs = self.get_vtex_specs()

        for attribute_code in self.attributes:
            if attribute_code in attributes_to_push:
                self.process_attribute(attribute_code)

        self.process_90_day()
        self.update_name()

        if self.vtex_product_needs_update:
            url = f"{vtex_url_base}/catalog/pvt/product/{self.vtex_product_id}"
            self.vtex_product = call_service(url, vtex_headers, "PUT", self.vtex_product)

    def process_attribute(self, attribute_code):
        attribute_data = self.attributes[attribute_code]

        if attribute_code == "OVERSIZE_FEE":
            self.process_oversize_fee()
            return
        elif attribute_code == "LOCAL_UPC":
            self.process_upc(attribute_data)
            return
        elif attribute_code == "AUX6":  # Avalara Tax Code
            self.vtex_product["TaxCode"] = Attribute.get_data_value(attribute_data)
            self.vtex_product_needs_update = True
            return
        elif attribute_code == "UDF1" or attribute_code == "UDF5":
            if "UDF5" in self.attributes:  # presale date check
                self.vtex_product["ReleaseDate"] = self.get_earliest_date("UDF5")  # presale
            else:
                self.vtex_product["ReleaseDate"] = self.get_earliest_date("UDF1")  # launch

            self.vtex_product_needs_update = True
            return
        elif attribute_code == "PROP65_WWG":
            if not self.product:
                self.product = self.build_parent_product()
            self.sync_attribute(False, attribute_code, attribute_data, self.product)
            return
        elif attribute_code == "PROMO_EXCLUSION":
            self.process_promo_exclusion(attribute_data)
            return

        if attribute_code not in attribute_code_to_object:
            logger.warning(f"Attribute {attribute_code} does not exist in VTEX! Skipping this attribute.")
            return

        self.sync_attribute(True, attribute_code, attribute_data, self)

    def build_parent_product(self):
        url = f"{akeneo_url_base}/rest/v1/product-models/{self.data['parent']}"
        response = call_service(url, akeneo_headers, "GET")
        product = Product(response)
        product.vtex_specs = product.get_vtex_specs()
        return product

    def process_oversize_fee(self):
        self.vtex_sku_context = self.get_vtex_sku_context()
        for service in self.vtex_sku_context["Services"]:
            if service["Name"] == "Oversized fee":
                return  # this service already exists on this sku

        url = f"{vtex_url_base}/catalog/pvt/skuservice"
        body = copy.deepcopy(VTEX_OVERSIZE_FEE_BODY)
        body["SkuId"] = self.vtex_id
        response = requests.post(url, vtex_headers, json=body)  # todo: this could have better error handling
        if response.status_code == 400:
            response_json = response.json()
            if "Message" in response_json and response_json["Message"] == "This sku already has this Service":
                return

    def process_upc(self, upc_data):
        upc = Attribute.get_data_value(upc_data)
        url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}/ean/{upc}"
        call_service(url, vtex_headers, "POST")

    def process_promo_exclusion(self, attribute_data):
        # if any model's sku has a value of true, send true. If all values have value of false, send false
        if not Attribute.get_data_value(attribute_data):
            for sibling_sku in self.akeneo_sibling_skus:
                if "PROMO_EXCLUSION" in sibling_sku["values"] and Attribute.get_data_value(
                        sibling_sku["values"]["PROMO_EXCLUSION"]):
                    attribute_data = sibling_sku["values"]["PROMO_EXCLUSION"]
                    break

        if not self.product:
            self.product = self.build_parent_product()
        self.sync_attribute(False, "PROMO_EXCLUSION", attribute_data, self.product)

    def process_90_day(self):
        # if any model's sku has a value true, send true. Otherwise, remove the value in VTEX
        code_90_day = "90_DAY"

        if not self.product:
            self.product = self.build_parent_product()

        for sibling_sku in self.akeneo_sibling_skus:
            if code_90_day in sibling_sku["values"] and Attribute.get_data_value(sibling_sku["values"][code_90_day]):
                self.sync_attribute(False, code_90_day, sibling_sku["values"][code_90_day], self.product)
                return

        delete_data = [{"locale": None, "scope": None, "data": "#N/A#"}]
        self.sync_attribute(False, code_90_day, delete_data, self.product)

    def get_akeneo_sibling_skus(self):
        if self.data["parent"]:
            return self.get_skus_of_model(self.data["parent"])
        else:
            return [self.data]

    def update_name(self):
        # todo: later add boolean to determine whether we need to get specs (if they weren't updated, no need)
        self.vtex_specs = self.get_vtex_specs()  # called here so we get the latest specs, which were just updated

        name = f"{self.akeneo_id}-"
        for spec_id in self.vtex_specs:
            name = f"{name} {self.vtex_specs[spec_id].value}"
        # todo: add name.trim()
        if self.vtex_sku["Name"] == name:
            return
        self.vtex_sku["Name"] = name

        url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}"
        self.vtex_sku = call_service(url, vtex_headers, "PUT", self.vtex_sku)

    @staticmethod
    def sync_attribute(must_be_sku_attribute, attribute_code, attribute_data, item):
        base_attribute = attribute_code_to_object[attribute_code]

        if must_be_sku_attribute and not base_attribute.item_type == "SKU":
            return

        attribute = Attribute(attribute_code, base_attribute.label, base_attribute.id,
                              data=attribute_data, attribute_type=base_attribute.attribute_type,
                              item_type=base_attribute.item_type)
        attribute.sync(item)

    @staticmethod
    def get_skus_of_model(model_code):
        skus = []

        search_query = {"parent": [
            {"operator": "=",
             "value": model_code}
        ]}
        url = f"{akeneo_url_base}/rest/v1/products?search={json.dumps(search_query)}&limit=100"

        response = call_service(url, akeneo_headers, "GET")
        for sibling_sku in response["_embedded"]["items"]:
            skus.append(sibling_sku)

        while "next" in response["_links"]:
            response = call_service(response["_links"]["next"]["href"], akeneo_headers, "GET")
            for sibling_sku in response["_embedded"]["items"]:
                skus.append(sibling_sku)

        return skus

    def get_vtex_sku_context(self):
        url = f"{vtex_url_base}/catalog_system/pvt/sku/stockkeepingunitbyid/{self.vtex_id}"
        return call_service(url, vtex_headers, "GET")

    def get_vtex_specs(self):
        vtex_specs = {}

        url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}/specification"
        spec_response = call_service(url, vtex_headers, "GET")

        for spec in spec_response:
            vtex_specs[spec["FieldId"]] = Specification(spec["Id"], spec["Text"])

        return vtex_specs

    @staticmethod
    def get_vtex_sku(akeneo_id) -> dict:
        vtex_url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit?refId={akeneo_id}"
        response = call_service(vtex_url, vtex_headers, "GET", handle_404=True)
        if response:
            return response
        else:
            return {"Id": None}

    def set_active_status(self):
        if "IsActive" in self.vtex_sku and self.vtex_sku["IsActive"] == self.is_active:
            # No need to update as the flag hasn't changed
            return

        if not self.vtex_assets:  # SKUs can only be active in VTEX if there is an asset
            self.is_active = False

        update_sku_url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}"
        self.vtex_sku["IsActive"] = self.is_active
        self.vtex_sku["ActivateIfPossible"] = False

        call_service(update_sku_url, vtex_headers, "PUT", self.vtex_sku)

    def process_assets(self):
        self.vtex_assets = self.get_vtex_assets()

        asset_attributes = akeneo_asset_attributes["HERO"] + akeneo_asset_attributes["MAIN"] \
                           + akeneo_asset_attributes["ALTERNATE"]
        self.process_asset_removals(asset_attributes)
        for asset_attribute in asset_attributes:
            if asset_attribute in self.attributes:
                asset_attribute_data = Attribute.get_data_value(self.attributes[asset_attribute])
                self.process_asset_additions(asset_attribute_data, asset_attribute)

        self.vtex_assets = self.get_vtex_assets()

    def get_vtex_assets(self, attempt=1):
        assets = set()
        url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}/file"
        response = requests.get(url=url, headers=vtex_headers)
        if response.ok:
            for asset in response.json():
                name_index = asset["Name"].find('_')  # VTEX prepends a string ending with _ to the akeneo asset id
                akeneo_asset_id = asset["Name"][name_index + 1:]
                assets.add(akeneo_asset_id)

            return assets
        elif response.status_code == 404:
            return assets
        else:
            if attempt < number_of_attempts:
                return self.get_vtex_assets(attempt + 1)
            else:
                raise ItemException(f"Failed to get a successful VTEX SKU File response after {number_of_attempts} "
                                    f"attempts. Response: {response.status_code} {response.reason}")

    def process_asset_additions(self, asset_attribute_data, asset_attribute):
        for akeneo_asset_id in asset_attribute_data:
            if akeneo_asset_id not in self.vtex_assets:
                logger.info(f"Adding Asset: {akeneo_asset_id}")
                is_hero = asset_attribute in akeneo_asset_attributes["HERO"] and not self.product_has_hero_image()

                if is_hero:
                    is_main = True
                else:
                    is_main = asset_attribute in akeneo_asset_attributes["MAIN"]

                akeneo_family_code = asset_attribute_to_family_code[asset_attribute]

                asset = Asset(akeneo_asset_id, akeneo_family_code)
                asset.filename = asset.get_akeneo_asset()
                asset.add_to_vtex(self, is_main, is_hero)

    def process_asset_removals(self, asset_attributes):
        akeneo_asset_ids = set()
        for asset_attribute in asset_attributes:
            if asset_attribute in self.attributes:
                asset_attribute_data = Attribute.get_data_value(self.attributes[asset_attribute])
                for akeneo_asset_id in asset_attribute_data:
                    akeneo_asset_ids.add(akeneo_asset_id.lower())

        for vtex_asset_id in self.vtex_assets:
            if vtex_asset_id.lower() not in akeneo_asset_ids:
                logger.info(f"VTEX Asset {vtex_asset_id} is not in Akeneo. Deleting all VTEX assets.")
                url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{self.vtex_id}/file"
                call_service(url, vtex_headers, "DELETE")
                self.vtex_assets = set()
                return

    def product_has_hero_image(self):
        # if the parent product has a hero image in any of its SKUs, we can't add another
        if not self.vtex_sibling_sku_files:
            self.vtex_sibling_sku_files = self.get_vtex_sibling_sku_files()

        for sku_id in self.vtex_sibling_sku_files:
            if "Hero" in self.vtex_sibling_sku_files[sku_id] or "hero" in self.vtex_sibling_sku_files[sku_id]:
                return True

        return False

    def get_vtex_sibling_sku_files(self):
        vtex_sibling_sku_files = {}

        skus_url = f"{vtex_url_base}/catalog_system/pub/products/variations/{self.vtex_product_id}"
        skus_response = requests.get(url=skus_url, headers=vtex_headers)

        if skus_response.ok:
            skus_response = skus_response.json()
            for sku_data in skus_response["skus"]:
                sku_id = sku_data["sku"]
                sku_files_labels = []

                file_url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{sku_id}/file"
                file_response = call_service(file_url, vtex_headers, "GET", handle_404=True)
                if file_response:
                    for file in file_response:
                        sku_files_labels.append(file["Label"])

                vtex_sibling_sku_files[sku_id] = sku_files_labels
        elif skus_response.status_code != 404:
            logger.warning(f"{skus_response.status_code} {skus_response.content}")

        return vtex_sibling_sku_files

    def process_sku(self):
        skus_processed.add(self.akeneo_id)
        SKU.total_count += 1

        # Process only (active in Akeneo) OR (inactive in Akeneo but active in VTEX)
        if self.is_active:
            logger.info(f"{SKU.total_count}|Processing Akeneo SKU ID {self.akeneo_id}")
            self.akeneo_sibling_skus = self.get_akeneo_sibling_skus()

            self.action()
            self.process_attributes()
            self.process_assets()

            self.set_active_status()
            self.process_sibling_skus()
            return

        elif not self.is_active and "IsActive" in self.vtex_sku and self.vtex_sku["IsActive"]:
            logger.info(f"{SKU.total_count}|Deactivating Akeneo SKU ID {self.akeneo_id}")
            self.akeneo_sibling_skus = self.get_akeneo_sibling_skus()

            self.set_active_status()
            self.process_sibling_skus()
            return

        raise InactiveItemException

    def process_sibling_skus(self):
        for sku_data in self.akeneo_sibling_skus:
            SKU.update_sku(sku_data)

    @staticmethod
    def update_sku(sku_data, attempt=1, is_reprocess=False):
        needs_reprocessing = False
        try:
            SKU(sku_data).process_sku()
            SKU.success_count += 1
            return
        except InactiveItemException:
            return
        except (ItemException, ChildProcessError) as ie:
            error = ie
            needs_reprocessing = True
        except KeyError as ke:
            error = f"KeyError on Key {ke}"
        except requests.exceptions.RequestException as re:
            logger.error(re)
            time.sleep(10)
            if attempt < number_of_attempts:
                SKU.update_sku(sku_data, attempt + 1)
                return
            error = re

        if needs_reprocessing and not is_reprocess:
            skus_to_reprocess.append(sku_data)
        logger.error(error)
        item_errors.add(f"{error}[|]{sku_data['identifier']}")
        SKU.error_count += 1
        SKU.not_processed.add(sku_data["identifier"])

    @staticmethod
    def update_skus(url):
        try:
            search_after_value = url[url.index('search_after=') + len('search_after='):]
        except ValueError:
            search_after_value = "Initial"
        logger.info(f"SKU Page {SKU.page_number}: {search_after_value}")

        skus_response = call_service(url, akeneo_headers, "GET")
        if "_embedded" in skus_response and "items" in skus_response["_embedded"]:
            skus = skus_response["_embedded"]["items"]
            for sku_data in skus:
                SKU.update_sku(sku_data)

        return skus_response["_links"]

    @staticmethod
    def update_all_skus(ref_date):
        logger.info("Update SKUs")
        # update recursion depth of the stack to above the original 1000 to allow the processing of all sibling skus
        sys.setrecursionlimit(recursion_depth)

        if full_data_load:
            url = f"{akeneo_url_base}/rest/v1/products?pagination_type=search_after&limit=100"
            if search_after:
                url += f"&search_after={search_after}"
        else:
            search = {"updated": [{"operator": ">",
                                   "value": ref_date}]}
            url = f"{akeneo_url_base}/rest/v1/products?pagination_type=search_after" \
                  f"&search={json.dumps(search)}&limit=100"

        skus_links = SKU.update_skus(url)
        while "next" in skus_links:
            SKU.page_number += 1
            skus_links = SKU.update_skus(skus_links["next"]["href"])

        if skus_to_reprocess:
            logger.info(f"There are {len(skus_to_reprocess)} SKUs to reprocess. Waiting 5 minutes.")

            # subtract off skus_to_reprocess from skus_processed so they process again
            for sku_to_reprocess in skus_to_reprocess:
                skus_processed.remove(sku_to_reprocess["identifier"])

            time.sleep(300)
            for sku_data in skus_to_reprocess:
                SKU.update_sku(sku_data, is_reprocess=True)


class Asset:
    def __init__(self, akeneo_asset_id, akeneo_family_code):
        self.akeneo_asset_id = akeneo_asset_id
        self.akeneo_family_code = akeneo_family_code
        self.vtex_archive_id = None
        self.filename = None

    def add_to_vtex(self, sku: SKU, is_main, is_hero):
        vtex_sku_file_url = f"{vtex_url_base}/catalog/pvt/stockkeepingunit/{sku.vtex_id}/file"

        if is_hero:
            label = "Hero"
        else:
            label = ""

        vtex_sku_file_body = {
            "IsMain": is_main,
            "Label": label,
            "Name": self.filename,
            "Url": f"{asset_server_url_base}/asset/{self.filename}"
        }
        vtex_sku_file = call_service(vtex_sku_file_url, vtex_headers, "POST", vtex_sku_file_body)
        self.vtex_archive_id = vtex_sku_file["ArchiveId"]

    def get_akeneo_asset(self, attempt=1):
        asset_url = f"{akeneo_url_base}/rest/v1/asset-families/{self.akeneo_family_code}/assets/{self.akeneo_asset_id}"
        asset_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {akeneo_token}"}
        asset_link = call_service(asset_url, asset_headers, "GET")
        image_url = asset_link["values"]["media"][0]["_links"]["download"]["href"]

        akeneo_image_headers = {"Accept": "*/*", "Authorization": f"Bearer {akeneo_token}"}
        filename = image_url.split('/')[-1]
        file_path = f"./storage/{filename}"

        image_response = requests.get(url=image_url, headers=akeneo_image_headers, stream=True)
        if image_response.ok:
            # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
            image_response.raw.decode_content = True

            with open(file_path, 'wb') as f:
                shutil.copyfileobj(image_response.raw, f)

            return filename
        elif image_response.status_code == 401:
            if attempt < number_of_attempts:
                set_auth()
                return self.get_akeneo_asset(attempt + 1)
        raise ItemException("Bad Image Response: " + str(image_response.status_code) + " " + image_response.reason)


class Product:
    total_count = 0
    success_count = 0
    error_count = 0
    not_processed = set()
    page_number = 1

    def __init__(self, data, dummy=False):
        self.akeneo_code = data["code"]
        if test_model_ids and self.akeneo_code not in test_model_ids:
            raise InactiveItemException

        if data["parent"]:
            logger.info(f"Model {self.akeneo_code} is not at the top level, so it will not go to VTEX.")
            return

        self.data = data
        self.vtex_product = self.get_vtex_product(self.akeneo_code)
        self.vtex_id = self.vtex_product["Id"]
        self.attributes = data["values"]
        self.is_active = self.product_is_active()
        self.category = self.get_category()
        self.vtex_specs = {}  # Map of FieldId to list of Specification objects
        self.dummy = dummy  # If this is a 'dummy' product for a standalone/simple SKU in Akeneo

    @staticmethod
    def build_brand_map():
        logger.info("Building the Brand Map")

        url = f"{vtex_url_base}/catalog_system/pvt/brand/list"
        brand_response = call_service(url, vtex_headers, "GET")
        for brand in brand_response:
            brand_name_to_id[brand["name"].lower()] = brand["id"]

    @staticmethod
    def add_brand(brand_name):
        logger.info(f"Adding Brand: {brand_name}")

        url = f"{vtex_url_base}/catalog/pvt/brand"
        method = "POST"
        data = copy.deepcopy(VTEX_BRAND_BODY)
        data["Name"] = brand_name
        data["Text"] = brand_name
        data["SiteTitle"] = brand_name

        brand_response = call_service(url, vtex_headers, method, data)
        brand_name_to_id[brand_name.lower()] = brand_response["Id"]

    @staticmethod
    def get_vtex_product(ref_id):
        vtex_url = f"{vtex_url_base}/catalog_system/pvt/products/productgetbyrefid/{ref_id}"
        response = call_service(vtex_url, vtex_headers, "GET", handle_404=True)
        if response:
            return response
        else:
            return {"Id": None}

    def product_is_active(self):
        if "WWG_WEB_READY_PRODUCT_MODEL" in self.attributes:
            return Attribute.get_data_value(self.attributes["WWG_WEB_READY_PRODUCT_MODEL"])
        else:
            return False

    def get_category(self):
        categories = self.data["categories"]
        for assigned_category in categories:
            if assigned_category in leaf_category_code_to_name:
                return assigned_category

        if self.is_active:
            raise ItemException(f"Product Model {self.akeneo_code} is not assigned to a leaf category. Skipping it")
        else:
            raise InactiveItemException

    def build_vtex_product_body(self):
        if self.vtex_id:
            # This product exists in VTEX
            body = self.vtex_product
        else:
            # This product does not exist in VTEX
            body = copy.deepcopy(VTEX_PRODUCT_BODY)
            body["RefId"] = self.akeneo_code

        if "WEBSITE_PRODUCT" in self.attributes:
            body["Name"] = Attribute.get_data_value(self.attributes["WEBSITE_PRODUCT"])
            body["Title"] = Attribute.get_data_value(self.attributes["WEBSITE_PRODUCT"])
        else:
            if not body["Name"]:
                raise ItemException(f"Akeneo Model {self.akeneo_code} has no 'Website Product Title'")

        if "DESCR" in self.attributes:
            body["DescriptionShort"] = Attribute.get_data_value(self.attributes["DESCR"])
        if "FULLDESCR" in self.attributes:
            body["Description"] = Attribute.get_data_value(self.attributes["FULLDESCR"])
        if "SEO_META_DESCRIPTION" in self.attributes:
            body["MetaTagDescription"] = Attribute.get_data_value(self.attributes["SEO_META_DESCRIPTION"])
        if "KEYWORDS" in self.attributes:
            body["KeyWords"] = Attribute.get_data_value(self.attributes["KEYWORDS"])

        if "CLEAN_URL" in self.attributes:
            body["LinkId"] = Attribute.get_data_value(self.attributes["CLEAN_URL"])
        else:
            raise ItemException(f"Akeneo Model {self.akeneo_code} has no 'Clean URL'")

        # Brand
        if "WWG_BRAND" in self.attributes:
            brand_code = Attribute.get_data_value(self.attributes["WWG_BRAND"])
            brand_name = akeneo_attribute_options["WWG_BRAND"][brand_code]

            if brand_name.lower() not in brand_name_to_id:
                logger.info(f"Brand: '{brand_name}' does not exist in VTEX. Creating it")
                self.add_brand(brand_name)

            body["BrandId"] = brand_name_to_id[brand_name.lower()]
        else:
            if not body["BrandId"]:
                logger.warning(f"Akeneo Model {self.akeneo_code} has no Brand. Setting to default in VTEX")
                body["BrandId"] = brand_name_to_id["default"]

        # Category
        category_name = leaf_category_code_to_name[self.category]
        if category_name in category_name_to_id:
            body["CategoryId"] = category_name_to_id[leaf_category_code_to_name[self.category]]
        else:
            raise ItemException(f"Category '{category_name}' does not exist in VTEX. Can't process this product.")

        return body

    def action(self):
        product_body = self.build_vtex_product_body()
        if self.vtex_id:
            # This product exists in VTEX
            url = f"{vtex_url_base}/catalog/pvt/product/{self.vtex_id}"
            method = "PUT"
        else:
            # This product does not exist in VTEX
            url = f"{vtex_url_base}/catalog/pvt/product"
            method = "POST"

        response = call_service(url, vtex_headers, method, product_body)
        self.vtex_product = response
        self.vtex_id = response["Id"]

    def process_attributes(self):
        self.vtex_specs = self.get_vtex_specs()

        for attribute_code in self.attributes:
            if attribute_code in attributes_to_push:
                attribute_data = self.attributes[attribute_code]  # todo: are these all the attributes [below] ?
                if attribute_code not in attribute_code_to_object and attribute_code not in ["AUX6", "LOCAL_UPC"]:
                    logger.warning(f"Attribute {attribute_code} does not exist in VTEX! Skipping this attribute.")
                    continue

                base_attribute = attribute_code_to_object[attribute_code]
                if base_attribute.item_type == "PRODUCT":
                    attribute = Attribute(attribute_code, base_attribute.label, base_attribute.id,
                                          data=attribute_data, attribute_type=base_attribute.attribute_type,
                                          item_type=base_attribute.item_type)
                    attribute.sync(self)

    def get_vtex_specs(self):
        vtex_specs = {}

        url = f"{vtex_url_base}/catalog/pvt/product/{self.vtex_id}/specification"
        spec_response = call_service(url, vtex_headers, "GET")

        for spec in spec_response:
            if spec["FieldId"] in vtex_specs:
                vtex_specs[spec["FieldId"]].append(Specification(spec["Id"], spec["Text"]))
            else:
                vtex_specs[spec["FieldId"]] = [Specification(spec["Id"], spec["Text"])]

        return vtex_specs

    def set_active_status(self):
        is_visible = self.is_visible()

        if "IsActive" in self.vtex_product and self.vtex_product["IsActive"] == self.is_active \
                and "IsVisible" in self.vtex_product and self.vtex_product["IsVisible"] == is_visible:
            # No need to update as the active and visible flags haven't changed
            return

        update_sku_url = f"{vtex_url_base}/catalog/pvt/product/{self.vtex_id}"
        self.vtex_product["IsActive"] = self.is_active
        self.vtex_product["IsVisible"] = is_visible

        call_service(update_sku_url, vtex_headers, "PUT", self.vtex_product)

    def is_visible(self):
        presale_date, launch_date = self.get_earliest_dates()

        if not presale_date and not launch_date:
            return True
        elif launch_date and not presale_date:
            return datetime.now() >= launch_date
        elif launch_date and presale_date:
            return datetime.now() >= presale_date
        else:
            raise ItemException(f"Akeneo Model {self.akeneo_code} has a Presale Date but no Launch Date")

    def get_earliest_dates(self):
        earliest_presale_date = None
        earliest_launch_date = None

        if self.dummy:
            child_skus = [self.data]
        else:
            child_skus = SKU.get_skus_of_model(self.akeneo_code)

        for child_sku in child_skus:
            if "UDF1" in child_sku["values"]:
                launch_date_raw = Attribute.get_data_value(child_sku["values"]["UDF1"])
                launch_date = datetime.strptime(launch_date_raw, "%Y-%m-%dT%H:%M:%S+00:00")
                if earliest_launch_date:
                    if launch_date < earliest_launch_date:
                        earliest_launch_date = launch_date
                else:
                    earliest_launch_date = launch_date

            if "UDF5" in child_sku["values"]:
                presale_date_raw = Attribute.get_data_value(child_sku["values"]["UDF5"])
                presale_date = datetime.strptime(presale_date_raw, "%Y-%m-%dT%H:%M:%S+00:00")
                if earliest_presale_date:
                    if presale_date < earliest_presale_date:
                        earliest_presale_date = presale_date
                else:
                    earliest_presale_date = presale_date

        return earliest_presale_date, earliest_launch_date

    def process_model(self):
        if self.is_active:
            logger.info(f"{Product.total_count}|Processing Akeneo Model Code: {self.akeneo_code}")
            self.action()
            self.process_attributes()
            self.set_active_status()
            return

        elif "IsActive" in self.vtex_product and self.vtex_product["IsActive"]:
            logger.info(f"{Product.total_count}|Inactivating in VTEX: Akeneo Model Code {self.akeneo_code}")
            self.set_active_status()
            return

        raise InactiveItemException

    @staticmethod
    def update_product(product_data, attempt=1):
        try:
            Product.total_count += 1
            Product(product_data).process_model()
            Product.success_count += 1
            return
        except InactiveItemException:
            return
        except (ItemException, ChildProcessError) as ie:
            error = ie
        except KeyError as ke:
            error = f"KeyError on Key {ke}"
        except requests.exceptions.RequestException as re:
            logger.error(re)
            time.sleep(10)
            if attempt < number_of_attempts:
                Product.update_product(product_data, attempt + 1)
                return
            error = re

        logger.error(error)
        item_errors.add(f"{error}[|]{product_data['code']}")
        Product.error_count += 1
        Product.not_processed.add(product_data["code"])

    @staticmethod
    def update_products(url):
        search_after_value = ""
        if full_data_load:
            try:
                search_after_value = url[url.index('search_after=') + len('search_after='):]
            except ValueError:
                search_after_value = "Initial"
        logger.info(f"Product Model Page {Product.page_number}: {search_after_value}")

        products_response = call_service(url, akeneo_headers, "GET")
        if "_embedded" in products_response and "items" in products_response["_embedded"]:
            products = products_response["_embedded"]["items"]
            for product_data in products:
                Product.update_product(product_data)

        return products_response["_links"]

    @staticmethod
    def update_all_products(ref_date):
        logger.info("Update Products (Models)")
        if full_data_load:
            url = f"{akeneo_url_base}/rest/v1/product-models?pagination_type=search_after&limit=100"
            if search_after:
                url += f"&search_after={search_after}"
        else:
            search = {"updated": [{"operator": ">",
                                   "value": ref_date}]}
            url = f"{akeneo_url_base}/rest/v1/product-models?search={json.dumps(search)}" \
                  f"&pagination_type=search_after&limit=100"

        models_links = Product.update_products(url)
        while "next" in models_links:
            Product.page_number += 1
            models_links = Product.update_products(models_links["next"]["href"])


def setup_logger(filename):
    timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    log_format = "%(asctime)s|%(levelname)s|%(threadName)s|%(message)s"
    global log_filename
    log_filename = f"./logs/{filename}_{timestamp}.log"

    logging.basicConfig(format=log_format)
    formatter = logging.Formatter(log_format)
    logger.setLevel(logging.INFO)

    info_log_channel = logging.FileHandler(log_filename)
    info_log_channel.setLevel(logging.INFO)
    info_log_channel.setFormatter(formatter)
    logger.addHandler(info_log_channel)


def call_service(url, headers, method, data=None, params=None, attempt=1, handle_404=False):
    response = requests.request(method=method, url=url, headers=headers, json=data, params=params)
    if response.ok:
        try:
            return response.json()
        except json.decoder.JSONDecodeError:
            return {}
    elif 400 <= response.status_code < 500 and response.status_code != 429:  # 429 is a rate limit response
        if response.status_code == 401:  # 401 is an invalid authentication response
            set_auth()
            # todo: clear everything older than n minutes in the ./storage folder
            return call_service(url, akeneo_headers, method, data, params, attempt + 1)
        elif handle_404 and response.status_code == 404:
            return None
        else:
            raise ChildProcessError(
                f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
    else:
        logger.error(f"{response.status_code}|{response.reason}|{response.content}|{method}|{url}|{data}")
        if attempt < number_of_attempts:
            logger.info(f"Attempt {attempt}. Waiting {retry_wait} seconds and then retrying: {url}")
            time.sleep(retry_wait)
            return call_service(url, headers, method, data, params, attempt + 1)
        else:
            raise ChildProcessError(
                f"Unable to get a successful response after {number_of_attempts} attempts on: {url}")


def set_auth():
    logger.info("Authenticating with Akeneo")

    global akeneo_token
    global akeneo_headers

    auth_response = call_service(auth_url, auth_headers, "POST", data=auth_body)
    akeneo_token = auth_response["access_token"]
    akeneo_headers = {"Content-Type": "application/json", "Authorization": f"Bearer {akeneo_token}"}


def create_attachment(file_name: str) -> MIMEBase:
    part = MIMEBase("application", "octet-stream")

    with open(file_name, "rb") as attachment:
        part.set_payload(attachment.read())

    encoders.encode_base64(part)
    part.add_header(
        "Content-Disposition", f"attachment; filename= {file_name}",
    )

    return part


def send_report(is_success: bool):
    logger.info("Sending the results in an email")
    if is_success:
        subject = f"Integrator Results - {vtex_env}"
    else:
        subject = f"Critical Integrator Error - {vtex_env}"

    body = f"The following were sent to VTEX. SKUs: {SKU.success_count}. Products: {Product.success_count}"

    message = MIMEMultipart()
    message["From"] = sender_address
    message["To"] = ",".join(receiver_addresses)
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain"))

    message.attach(create_attachment(log_filename))

    if item_errors:
        logger.info(f"Attaching the error report with {len(item_errors)} errors.")

        timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
        error_report_filename = f"./logs/integrator_error_report_{timestamp}.txt"
        with open(error_report_filename, "w") as error_file:
            error_file.write("Error Message[|]Item Number\n")
            for error in item_errors:
                error_file.write(f"{error}\n")

        message.attach(create_attachment(error_report_filename))
    else:
        logger.info(f"There is no error report to attach.")

    text = message.as_string()
    with smtplib.SMTP(mail_server, mail_port) as server:
        server.starttls()
        server.login(sender_address, sender_password)
        server.sendmail(sender_address, receiver_addresses, text)


def main():
    start_time = datetime.now()
    start_time_formatted = start_time.strftime('%Y-%m-%d %H:%M:%S')
    setup_logger("integrator")
    logger.info(INFOVERITY)
    set_auth()
    logger.info(f"Begin Integrator. Akeneo {akeneo_env} -> VTEX {vtex_env}")

    Product.build_brand_map()
    Category.get_leaf_categories()
    Category.get_vtex_categories()
    Attribute.build_system_attribute_data()

    if full_data_load:
        ref_date = None
    else:
        with open(ref_date_file, 'r') as file:
            ref_date = file.read().strip()

        logger.info(f"Updating all Akeneo Items that have changed since: {ref_date}")

    logger.info(f"Let's Get Truckin! {truck}")
    Product.update_all_products(ref_date)
    SKU.update_all_skus(ref_date)

    logger.info(f"Number of Total Products Processed, including Inactive: {Product.total_count}")
    logger.info(f"Number of Products Successfully Synced to VTEX: {Product.success_count}")
    logger.info(f"Number of Products Processed with Errors: {Product.error_count}")
    logger.info(f"Products NOT fully Processed: {Product.not_processed}")
    logger.info(f"Number of Total SKUs Processed, including Inactive: {SKU.total_count}")
    logger.info(f"Number of SKUs Successfully Synced to VTEX: {SKU.success_count}")
    logger.info(f"Number of SKUs Processed with Errors: {SKU.error_count}")
    logger.info(f"SKUs NOT fully Processed: {SKU.not_processed}")
    logger.info(f"Runtime: {datetime.now() - start_time}")

    logger.info(f"Setting the reference date to {start_time_formatted}")
    with open(ref_date_file, 'w') as file:
        file.write(start_time_formatted)


if __name__ == '__main__':
    try:
        main()
        success = True
    except Exception as e:
        logger.critical(e)
        success = False

    send_report(success)
    logger.info("Integrator Complete.")
    os.kill(os.getpid(), signal.SIGKILL)
