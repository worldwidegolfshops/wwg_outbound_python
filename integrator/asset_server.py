from fastapi import FastAPI
from fastapi.responses import FileResponse

app = FastAPI()


@app.get("/status")
async def status():
    return "We Are Truckin!"


@app.get("/asset/{file_name}")
async def get_asset(file_name: str):
    return FileResponse(f"/app/storage/{file_name}")
