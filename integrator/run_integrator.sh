#!/bin/bash
echo "Begin Integrator"

source "/var/www/vhosts/api.budgetgolf.com/.bash_profile"

for i in {1..23}
do
  if [ "$(docker ps -q -f name=integrator_integrator_1)" ]; then
    if [ "$i" -lt 23 ]; then
      echo "The previous Integrator execution is still running. Waiting one hour and then checking again. Attempt $i"
      sleep 1h
    else
      echo 'The previous Integrator execution is still running after 23 attempts. Exiting.'
      exit
    fi
  else
    echo "The previous Integrator execution is not running. Continue"
    break
  fi
done

echo "Change Directory to the Integrator Directory"
cd "/var/www/vhosts/api.budgetgolf.com/integrator" || exit

echo "Remove all assets stored by the previous run"
rm -f ./storage/*

echo "Ensure the last run is stopped"
/usr/local/bin/docker-compose down
echo "Start a new run."
/usr/local/bin/docker-compose up --abort-on-container-exit

echo "Delete all log and error files older than 333 days"
find ./logs/* -mtime +333 -exec rm -f {} \;