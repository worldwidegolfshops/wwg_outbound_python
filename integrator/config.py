import os
import logging


# CONFIG
full_data_load = False
# full_data_load = True

# search_after = "EsaCqixi"
search_after = None

# test_sku_ids = ["6006407"]
test_sku_ids = None

# test_model_ids = ["100012279"]
test_model_ids = None


# CONSTANTS
vtex_env = os.getenv("VTEX_ENV")
if vtex_env == "QA":
    vtex_url_base = "https://worldwidegolfqa.myvtex.com/api"
    app_key = os.getenv("VTEX_APP_KEY")
    app_token = os.getenv("VTEX_APP_TOKEN")
elif vtex_env == "Production":
    vtex_url_base = "https://worldwidegolf.myvtex.com/api"
    app_key = os.getenv("VTEX_APP_KEY_PROD")
    app_token = os.getenv("VTEX_APP_TOKEN_PROD")
else:
    raise ChildProcessError(f"VTEX Environment '{vtex_env}' not valid. Valid values are: QA, Production")

vtex_headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-VTEX-API-AppKey": app_key,
    "X-VTEX-API-AppToken": app_token
}

akeneo_env = os.getenv("AKENEO_ENV")
if akeneo_env == "Sandbox":
    akeneo_url_base = "https://wwgs-sandbox.cloud.akeneo.com/api"
    akeneo_username = os.getenv("WWGS_Akeneo_Username")
    akeneo_password = os.getenv("WWGS_Akeneo_Password")
    akeneo_auth = os.getenv("WWGS_Base_64")
elif akeneo_env == "Production":
    akeneo_url_base = "https://wwgs.cloud.akeneo.com/api"
    akeneo_username = os.getenv("WWGS_Akeneo_Username_PROD")
    akeneo_password = os.getenv("WWGS_Akeneo_Password_PROD")
    akeneo_auth = os.getenv("WWGS_Base_64_PROD")
else:
    raise ChildProcessError(f"Akeneo Environment '{akeneo_env}' not valid. Valid values are: Sandbox, Production")

auth_url = f"{akeneo_url_base}/oauth/v1/token"
auth_headers = {"Content-Type": "application/json", "Authorization": akeneo_auth}
auth_body = {"grant_type": "password", "username": akeneo_username, "password": akeneo_password}

asset_server_url_base = "http://api.budgetgolf.com:9000"

mail_server = "smtp.gmail.com"
mail_port = 587
sender_address = "pim1@worldwidegolfshops.com"
sender_password = os.getenv("MAIL_PASSWORD")
receiver_addresses = ["pim@rdgolf.com", "ceft@infoverity.com"]

delete_attribute_value_1 = "#N/A#"
delete_attribute_value_2 = "N_A"
delete_attribute_value_3 = "<p>#N/A#</p>"

number_of_attempts = 5
retry_wait = 30  # Time in seconds to wait between retries

VTEX_PRODUCT_BODY = {
    "Name": None,
    "DepartmentId": None,
    "CategoryId": None,
    "BrandId": None,
    "LinkId": None,
    "RefId": None,
    "IsVisible": False,
    "Description": None,
    "DescriptionShort": None,
    "ReleaseDate": None,
    "KeyWords": None,
    "Title": None,
    "IsActive": False,
    "TaxCode": None,
    "MetaTagDescription": None,
    "SupplierId": None,
    "ShowWithoutStock": None,
    "Score": None
}

VTEX_SKU_BODY = {
    "ProductId": None,
    "IsActive": False,
    "Name": None,
    "RefId": None,
    "PackagedHeight": None,
    "PackagedLength": None,
    "PackagedWidth": None,
    "PackagedWeightKg": None,
    "Height": None,
    "Length": None,
    "Width": None,
    "WeightKg": None,
    "CubicWeight": None,
    "IsKit": None,
    "RewardValue": None,
    "ManufacturerCode": None,
    "CommercialConditionId": None,
    "MeasurementUnit": None,
    "UnitMultiplier": None,
    "KitItensSellApart": False,
    "ActivateIfPossible": False
}

VTEX_SPEC_BODY = {
    "FieldTypeId": None,
    "CategoryId": 0,
    "FieldGroupId": 41,
    "Name": None,
    "Description": None,
    "IsStockKeepingUnit": None,
    "IsActive": True
}

VTEX_VALUE_BODY = {
    "FieldId": None,
    "Name": None,
    "IsActive": True,
    "Position": 100
}

VTEX_BRAND_BODY = {
    "Name": None,
    "Text": None,
    "SiteTitle": None,
    "Active": True
}

VTEX_OVERSIZE_FEE_BODY = {
     "SkuServiceTypeId": 1,
     "SkuServiceValueId": 1,
     "SkuId": None,
     "Name": "Oversized fee",
     "Text": "Oversized fee",
     "IsActive": True
}

INFOVERITY = """
 _____  ____  _____  ________    ___   ____   ____  ________  _______     _____  _________  ____  ____  
|_   _||_   \|_   _||_   __  | .'   `.|_  _| |_  _||_   __  ||_   __ \   |_   _||  _   _  ||_  _||_  _| 
  | |    |   \ | |    | |_ \_|/  .-.  \ \ \   / /    | |_ \_|  | |__) |    | |  |_/ | | \_|  \ \  / /   
  | |    | |\ \| |    |  _|   | |   | |  \ \ / /     |  _| _   |  __ /     | |      | |       \ \/ /    
 _| |_  _| |_\   |_  _| |_    \  `-'  /   \ ' /     _| |__/ | _| |  \ \_  _| |_    _| |_      _|  |_    
|_____||_____|\____||_____|    `.___.'     \_/     |________||____| |___||_____|  |_____|    |______|   
                                                                                                        """

truck = """
                       ___________________________________________________________
                      |                 INFOVERITY DATA TRUCKERS                  |
                      |                   TRUTH IN INFORMATION                    |
                      |                                                           |  ~~~~~~
             _______  |  Listed on the Inc. 5000                                  |
            / _____ | |  Named a Best Place to Work by Inc. Magazine              |  ~~~~~~
           / /(__) || |  Named a Best Place to Work in IT by Computerworld        |
  ________/ / |OO| || |  Named a Best Place to Work by Business First             |  ~~~~~~
 |         |-------|| |  Named a Wonderful Workplace for Young Professionals      |
(|         |     -.|| |_______________________                                    |
 |  ____   \       ||_________||____________  |                   ____      ____  |
/| / __ \   |______||     / __ \   / __ \   | |                  / __ \    / __ \ |
\|| /  \ |_______________| /  \ |_| /  \ |__| |_________________| /  \ |__| /  \|_|
   | () |                 | () |   | () |                        | () |    | () |
    \__/                   \__/     \__/                          \__/      \__/
"""

stopped_truck = """
                       ___________________________________________________________
                      |                 INFOVERITY DATA TRUCKERS                  |
                      |                   TRUTH IN INFORMATION                    |
                      |                                                           |
             _______  |  Listed on the Inc. 5000                                  |
            / _____ | |  Named a Best Place to Work by Inc. Magazine              |
           / /(__) || |  Named a Best Place to Work in IT by Computerworld        |
  ________/ / |OO| || |  Named a Best Place to Work by Business First             |
 |         |-------|| |  Named a Wonderful Workplace for Young Professionals      |
(|         |     -.|| |_______________________                                    |
 |  ____   \       ||_________||____________  |                   ____      ____  |
/| / __ \   |______||     / __ \   / __ \   | |                  / __ \    / __ \ |
\|| /  \ |_______________| /  \ |_| /  \ |__| |_________________| /  \ |__| /  \|_|
   | () |                 | () |   | () |                        | () |    | () |
    \__/                   \__/     \__/                          \__/      \__/
"""

attribute_to_specification_type_sku = {'pim_catalog_identifier': 6, 'pim_catalog_metric': 6,
                                       'pim_catalog_number': 6,
                                       'pim_catalog_reference_data_multi_select': 5,
                                       'pim_catalog_reference_data_simple_select': 6,
                                       'pim_catalog_simpleselect': 6,
                                       'pim_catalog_multiselect': 5, 'pim_catalog_date': 6,
                                       'pim_catalog_textarea': 6,
                                       'pim_catalog_text': 6, 'pim_catalog_file': 6, 'pim_catalog_image': 6,
                                       'pim_catalog_price_collection': 6, 'pim_catalog_boolean': 6,
                                       'akeneo_reference_entity': 6, 'akeneo_reference_entity_collection': 6,
                                       'pim_catalog_asset_collection': 6}

attribute_to_specification_type_product = {'pim_catalog_identifier': 1, 'pim_catalog_metric': 1,
                                           'pim_catalog_number': 4,
                                           'pim_catalog_reference_data_multi_select': 7,
                                           'pim_catalog_reference_data_simple_select': 5,
                                           'pim_catalog_simpleselect': 5,
                                           'pim_catalog_multiselect': 7, 'pim_catalog_date': 1,
                                           'pim_catalog_textarea': 2,
                                           'pim_catalog_text': 1, 'pim_catalog_file': 1, 'pim_catalog_image': 1,
                                           'pim_catalog_price_collection': 1, 'pim_catalog_boolean': 6,
                                           'akeneo_reference_entity': 1, 'akeneo_reference_entity_collection': 1,
                                           'pim_catalog_asset_collection': 1}

additional_attributes_to_push = ["LOCAL_UPC", "AUX6", "UDF1", "PROP65_WWG", "PROMO_EXCLUSION"]
additional_attributes_with_options = ["WWG_BRAND"]

recursion_depth = 1999

ref_date_file = "./persistent/ref_date_file.txt"

# GLOBALS
logger = logging.getLogger()

akeneo_token = None
akeneo_headers = None

attributes_to_push = set()

field_to_value_to_id = {}  # Key is VTEX Field Id. Value is dict with key of actual value. Value is 'Field Value Id'

leaf_category_code_to_name = {}  # Category Code to Name/Label

attribute_code_to_object = {}

category_name_to_id = {}

brand_name_to_id = {}

akeneo_attribute_options = {}
akeneo_asset_attributes = {"HERO": [], "MAIN": [], "ALTERNATE": []}
asset_attribute_to_family_code = {}

item_errors = set()
skus_to_reprocess = []

skus_processed = set()
